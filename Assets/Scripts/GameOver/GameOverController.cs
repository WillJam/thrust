using System;
using UnityEngine;
public class GameOverController
{
	GameObject GameOverPanel;
	TextureController GameOverType;
	
	GameOverModel _model;
	GameOverView _view;
	SpriteController restartOrQuitType;
	SpriteController tip;

	public GameOverEvents Events = new GameOverEvents();
	
	public GameOverController (GameOverModel model)
	{
		_model= model;
	}
	
	public void Build()
	{
		GameOverPanel = new GameObject("GameOverPanel");
		
		GameOverType = new TextureController("GameOverType");
		GameOverType.Build(GameOverPanel, new Vector3(10000, 10000, 50), _model.gameOverSize, _model.gameOverTexture);
		GameOverType.Hide(0);
		_view = GameOverPanel.AddComponent<GameOverView>();

		restartOrQuitType = new SpriteController("GameOver/restartquit", "restartquit");
		restartOrQuitType.Build(GameOverPanel, new Vector3(10000, 10000, 50));
		restartOrQuitType.TweenAlpha(0,0);

		tip = _model.tips[_model.rand];
		tip.Build (GameOverPanel, new Vector3(10000, 10000, 50));

		_view.Events.Restart+= HandleRestart;
	}




	public void GameOver(int score)
	{
		GameOverType.Position(_model.gameOverPos);
		GameOverType.Show(0.2f);
		tip.Position(_model.tipPos);
		tip.TweenAlpha(1, 0.2f);
		restartOrQuitType.Position(_model.restartorquitPos);
		restartOrQuitType.TweenAlpha(1,0.25f);
		_view.SetScore(score);
		_view.gameOver = true;
	}

	void HandleRestart (object sender, EventArgs<GameOverController> e)
	{
		_view.gameOver = false;
		Hide();

		Events.OnRestart(this);
	}

	public void Hide()
	{
		restartOrQuitType.TweenAlpha(0,0.5f);
		GameOverType.Hide(0.5f);
		restartOrQuitType.TweenAlpha(0, 0.5f);
		tip.TweenAlpha(0,0);
	}

	public void Destroy()
	{
		_view.Destroy();
	}
}


