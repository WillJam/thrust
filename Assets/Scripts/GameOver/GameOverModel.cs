using System;
using UnityEngine;
using System.Collections.Generic;

public class GameOverModel
{
	public Texture gameOverTexture = (Texture) Resources.Load ("Textures/GameOver");
	public Vector3 gameOverPos = new Vector3(0.5f,0.65f,1);
	public Vector3 gameOverSize = new Vector3(0.65f,0.65f,1);
	public Vector3 restartorquitPos = new Vector3(0,-13,-1);
	public Vector3 tipPos = new Vector3(1,-11,-1);

	public int rand;

	public List<SpriteController> tips = new List<SpriteController>();

	public GameOverModel()
	{

		tips.Add(new SpriteController("GameOver/activewep", "tip"));
		tips.Add(new SpriteController("GameOver/AvoidSwarmer", "tip"));
		tips.Add(new SpriteController("GameOver/BlockerBeam", "tip"));
		tips.Add(new SpriteController("GameOver/BlockerWeakness", "tip"));
		tips.Add(new SpriteController("GameOver/hoarder", "tip"));
		tips.Add(new SpriteController("GameOver/hunter", "tip"));
		tips.Add(new SpriteController("GameOver/minis", "tip"));
		tips.Add(new SpriteController("GameOver/power", "tip"));
		tips.Add(new SpriteController("GameOver/powerlocation", "tip"));
		tips.Add(new SpriteController("GameOver/score", "tip"));
		tips.Add(new SpriteController("GameOver/stalker", "tip"));
		tips.Add(new SpriteController("GameOver/wepdrop", "tip"));
		tips.Add(new SpriteController("GameOver/wepspace", "tip"));
		rand = UnityEngine.Random.Range(0, tips.Count - 1);



	}
	
}


