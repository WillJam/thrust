// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using System;
public class GameOverEvents
{

	public event EventHandler<EventArgs<GameOverController>> Restart;
	public void OnRestart(GameOverController controller)
	{
		var handler = Restart;
		if(handler != null) handler(this, new EventArgs<GameOverController>(controller));
	}

	public event EventHandler<EventArgs<GameOverController>> Quit;
	public void OnQuit(GameOverController controller)
	{
		var handler = Quit;
		if(handler != null) handler(this, new EventArgs<GameOverController>(controller));
	}
}


