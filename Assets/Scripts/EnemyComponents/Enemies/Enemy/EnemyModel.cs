using System;
using System.Collections.Generic;
using UnityEngine;
public class EnemyModel
{
	
	public string texturePathName {set; get;}

	public string defaultShipPathName = "Enemies/";
	public string defaultEnemyTexture = "EnemyTexture";


	public AudioClip hitsound = (AudioClip) Resources.Load("Sounds/Explosions/slight_distant_explosion");
	public float hitvolume = 0.135f;
	
	public EnemyModel ()
	{
		texturePathName = defaultEnemyTexture;
	}
	public EnemyModel(string textureName)
	{
		texturePathName = textureName;
	}
}


