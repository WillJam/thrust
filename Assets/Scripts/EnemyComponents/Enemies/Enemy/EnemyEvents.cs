using System;
using UnityEngine;

public class EnemyEvents
{
	
	public event EventHandler<EventArgs<GameObject>> Hit;
	public void OnHit(object sender, GameObject damage)
	{
		var handler = Hit;
		if(Hit != null) handler(this, new EventArgs<GameObject>(damage));
	}
	
	public event EventHandler<EventArgs<EnemyController>> Dead;
	public void OnDeath(object sender, EnemyController e)
	{
		var handler = Dead;
		if(Dead != null) handler(this, new EventArgs<EnemyController>(e));
	}
}

