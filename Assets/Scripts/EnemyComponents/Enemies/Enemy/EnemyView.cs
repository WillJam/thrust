using System;
using System.Collections;
using UnityEngine;

public class EnemyView: MonoBehaviour
{
	
	public void Move(Vector3 newPos)
	{
		gameObject.transform.position = newPos;
	}
	
	public void Rotate(Vector3 newRot)
	{
		gameObject.transform.rotation = Quaternion.Euler(newRot);
	}
	
	public void Destroy()
	{
		transform.position = new Vector3(1000, 1000, 1000);
		if(gameObject.activeSelf)
		{
			StartCoroutine("WaitThenDestroy");
		}


	}

	IEnumerator WaitThenDestroy()
	{
		yield return new WaitForSeconds(0.1f);
		Destroy(gameObject);
	}
}


