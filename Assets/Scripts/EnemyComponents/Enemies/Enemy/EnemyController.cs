using System;
using UnityEngine;
//baseabstract class for all enemy types
public abstract class EnemyController
{
	public string enemyName;
	protected GameObject gameObject; 
	protected EnemyModel _model;
	protected EnemyView _view;

	protected GibPool _pool;
	
	protected Texture3DController texture; 
	
	public EnemyEvents Events = new EnemyEvents();

	SoundController sounds;


	
	public EnemyController (string name, EnemyModel model, GibPool pool)
	{
		enemyName = name;
		_model = model; 
		_pool = pool;
	}
	
	public virtual void Build(Vector3 position)
	{
		gameObject = new GameObject(enemyName);
		gameObject.transform.position = position;


		sounds = new SoundController();
		sounds.Build(gameObject,false,false);
		//build texture 
		texture = new Texture3DController(_model.defaultShipPathName + _model.texturePathName, enemyName);
		texture.Build(gameObject);
		
		texture.Events.TriggerEnter += HandleTriggerEnter;
		texture.Events.TriggerStay += HandleTriggerStay;
	}

	void HandleTriggerStay (object sender, EventArgs<GameObject> e)
	{
		Events.OnHit(sender, e.Value);
	}
	
	protected virtual void HandleTriggerEnter (object sender, EventArgs<GameObject> e)
	{
		sounds.RandomizePitch(0.9f, 1.10f);
		sounds.PlayShot(_model.hitsound, _model.hitvolume);
		Events.OnHit(sender, e.Value);
	}
	

	protected void Destroy(EnemyView view)
	{

		view.Destroy();
	}

	public void Remove()
	{
		GameObject.Destroy(gameObject);
	}

	protected float CalculateDamage(string projectileTag)
	{
		if(projectileTag.Equals("baselaser"))
		{
			texture.Flash();
			return 1.0f;
		} 
		else if(projectileTag.Equals("herbie"))
		{
			texture.Flash();
			return 1000000;
		}
		else if(projectileTag.Equals("beam1"))
		{
			texture.Flash();
			return 1f;
		}
		else if(projectileTag.Equals("machinebullet"))
		{
			texture.Flash();
			return 1f;
		}
		else if(projectileTag.Equals("SeekerShot"))
		{
			texture.Flash();
			return 5f;
		}
		else
		{
			//harmless 
			return 0.0f;
		}
	}
	
}

