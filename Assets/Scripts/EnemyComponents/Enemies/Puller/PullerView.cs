using System;
using UnityEngine;
public class PullerView : EnemyView
{
	
	PullerModel _model;
	public PullerEvents Events = new PullerEvents();
	
	bool homing = false;
	float scanSpeed;
	float sideWinderTime;
	float sideWinderSpeed;
	float spawnTime;
	Ray r;
	private Transform PlayerTransform;
	public void Build(PullerModel model)
	{ 
		_model = model;

		scanSpeed = _model.scanSpeed;
		PlayerTransform = _model.Player.ShipObject.transform;
		sideWinderTime = _model.sideWinderTime;
		sideWinderSpeed = _model.sideWinderSpeed;
		spawnTime= _model.spawnTime;
		Vector3 rotation = transform.rotation.eulerAngles;  
		rotation.z = UnityEngine.Random.Range(330.0f, 390.0f);
		Rotate(rotation);
	}
	
	void Update()
	{
		if(PlayerTransform == null)
		{
			Destroy(gameObject);
		}
		//do this only if it hasn't been spawned by a carrier? 
		float speed; 
		Vector3 position;
		if(spawnTime >= 0)
		{
			spawnTime -= Time.deltaTime;
			speed = _model.spawnSpeed; 
			position =  _model.ScanPosition;
		}
		else if (homing)
		{
			speed = _model.speed;
			_model.speed -= _model.acceleration * Time.deltaTime;
			position = PlayerTransform.position;
			position.y += 1;
		}
		//isscanning
		else {
			Events.OnReady();
			speed = 3f;
			position = _model.ScanPosition;
			UpdateScan();
		}

		iTween.MoveUpdate(gameObject, iTween.Hash("time", speed, "position", position));
	}

	void UpdateSideWinder()
	{
		var newPos = transform.position;
		//easing function
		newPos.x =  -(sideWinderSpeed *_model.sideWinderDirection)/2 * (float)(Math.Cos(Math.PI*Time.deltaTime/_model.sideWinderTime) - 1) +  newPos.x;
		transform.position = newPos;
		if(sideWinderTime > 0)
		{
			Debug.Log (sideWinderTime);
			sideWinderTime -= Time.deltaTime;
		}
		else
		{
			Debug.Log (_model.sideWinderDirection);
			_model.sideWinderDirection *= -1;
			sideWinderTime = _model.sideWinderTime;
		}
	}

	void UpdateScan()
	{
		Vector3 rotation = transform.rotation.eulerAngles; 
		if(rotation.z > _model.maxRightScan && rotation.z < _model.maxLeftScan - 10 && _model.scanDirection == 1)
		{
			_model.scanDirection = -1;
			scanSpeed = _model.scanSpeed;
		}
		
		if((rotation.z < _model.maxLeftScan && rotation.z > _model.maxRightScan + 10) && _model.scanDirection == -1)
		{
			_model.scanDirection = 1;
			scanSpeed = _model.scanSpeed;
		}
		rotation.z += scanSpeed *_model.scanDirection * Time.deltaTime;
		transform.rotation = Quaternion.Euler(rotation);

		if(_model.Player.collider != null)
		{
			r = new Ray(transform.position, transform.up * -100);
			RaycastHit hit = new RaycastHit(); 
			
			if(_model.Player.collider.Raycast(r, out hit, 100))
			{
				Events.OnLockOn();
				homing = true;
			}
		}
	}
 	Vector3 GetToScanPosition()
	{
		var newPos = transform.position;
		newPos.y = -3.5f * (float)( -Math.Pow( 2, -10 * Time.deltaTime/1f)  + 1 ) + transform.position.y;
		return newPos;
	}
}


