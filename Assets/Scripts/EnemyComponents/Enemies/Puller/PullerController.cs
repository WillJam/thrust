using System;
using UnityEngine;
public class PullerController : EnemyController
{
	public new  PullerModel _model;
	public new PullerView _view;

	private Texture3DController DetectorBeam; 
	public PullerController (string name, PullerModel model, GibPool pool) : base(name, model, pool)
	{
		_model = model;
	}


	public override void Build(Vector3 pos)
	{
		_model.ScanPosition = pos;
		_model.ScanPosition.y -= 15;
		_model.spawnTime = UnityEngine.Random.Range(0.5f, 1.5f);

		base.Build(pos);

		iTween.Init(gameObject);
		
		DetectorBeam = new Texture3DController(_model.detectorPathName, "Detector Beam");
		DetectorBeam.Build(gameObject);
		DetectorBeam.Position(_model.detectorBeamPosition);

		_view = gameObject.AddComponent<PullerView>();
		_view.Build(_model);

		_view.Events.LockOn += HandleLockOn;
		_view.Events.Ready += HandleReady;

		_model.isScanning = false;
		DetectorBeam.SetActive(false);
		Events.Hit += HandleHit;
		Events.Dead += HandleDeath;
	}

	void HandleReady (object sender, EventArgs e)
	{
		DetectorBeam.SetActive(true);
		_model.isScanning = true;
	} 

	void HandleLockOn (object sender, EventArgs e)
	{
		_model.isScanning = false;
		DetectorBeam.SetActive(false);
	}

	void HandleHit (object sender, EventArgs<GameObject> damage)
	{
		Events.OnDeath(sender, this);
	}

	void HandleDeath (object sender, EventArgs<EnemyController> e)
	{
		//look into pooling? 
		ExplosionController exlpo = new ExplosionController();
		exlpo.Build(gameObject.transform.position, _model.numGibs, _model.gibRadius, _model.gibExplosionForce, _pool);
		Destroy(_view);
	}	
}


