using System;
using UnityEngine;
public class PullerModel :EnemyModel
{
	
	public const string  path = "Puller/PullerModel"; 
	public string detectorPathName = "Enemies/Puller/DetectorBeam";
	public ShipController Player;
	public PullerModel (int difficulty, ShipController player) : base(path)
	{
		Player = player;
	}

	public float health = 3;

	public float spawnTime;
	public float spawnSpeed = 4.3f;

	public Vector3 detectorBeamPosition = new Vector3(0, -20, 0);

	public Vector3 ScanPosition;
	public float scanSpeed = 30.0f;
	public float scanDirection = 1;
	public bool isScanning = true;
	public float maxRightScan = 30;
	public float maxLeftScan = 330;
		
	public float speed = 5.0f;
	public float acceleration = 5.0f;

	public int sideWinderDirection = 1;
	public float sideWinderSpeed = 15.0f;
	public float sideWinderTime = 0.2f;

	public int numGibs = 30;
	public float gibRadius = 0.1f;
	public float gibExplosionForce = 550.0f; 


}


