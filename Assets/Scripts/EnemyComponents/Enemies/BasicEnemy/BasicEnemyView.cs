using System;
using UnityEngine;
public class BasicEnemyView : EnemyView
{
	
	//enemy AI
	BasicEnemyModel _model;
	public BasicEnemyEvents Events = new BasicEnemyEvents();
	float lowestPoint = -2;
	public void Build(BasicEnemyModel model)
	{
		_model = model;
	}
	
	public void Update()
	{
		//put AI logic here
		if(_model == null)
		{
			Debug.LogError("View not built");
		}
		UpdateMovement();
		UpdateShootAI();
	}

	
	void UpdateShootAI()
	{
		if(_model.shootEnabled)
		{
			//- - - - shoot stuff - - - -
			//wait a bit . . . 
			if(_model.shootDelay > 0)
			{
				_model.shootDelay -= Time.deltaTime;
			}
			else
			{
				//shoot . . . 
				Events.OnShoot();
			}
		}
	}
	
	void UpdateMovement()
	{
		Vector3 newPosition = transform.position; 
		Vector3 screenPosition = Camera.main.WorldToScreenPoint(newPosition);
		//Handle the lowest point the ship can move, moves forever for now, may need to disable/destroy to save memory or w/e		
		newPosition = LeftAndRightMoveAI(newPosition, screenPosition);
		
		Move(newPosition);
	}
	
	Vector3 LeftAndRightMoveAI(Vector3 newPosition, Vector3 screenPosition){
		//AI for getting on screen and moving left and right
		if(screenPosition.y < Screen.height - (Screen.height * .05f)){
			newPosition.x += Time.deltaTime * _model.speed * _model.direction;
		} else {
			newPosition.y -= Time.deltaTime * _model.crawlSpeed;
		}
		
		//if it gets to either edge, move down and change direction
		if(screenPosition.x > Screen.width || screenPosition.x < 0)
		{
			if(newPosition.y > lowestPoint)
			{
				_model.direction = _model.direction * -1;
				_model.onScreen = true;
				Events.OnReachedLowestPoint();
			}
			newPosition.y -= _model.dropDistance;
			//move a little away from the edge so it doesn't drop completely
			newPosition.x = transform.position.x + 2 *(_model.direction);
			_model.speed += _model.acceleration;
		}
		return newPosition;
	}
}


