using System;
using UnityEngine;

public class BasicEnemyModel : EnemyModel
{
	
	public const string  path = "BasicEnemy/Enemyship_model 2"; 
	public BasicEnemyModel(int diffnum) :base(path) 
	{
		SetDifficulty(diffnum);
	}
	
	void SetDifficulty(int difficulty)
	{
		if(difficulty == 0)
		{
			shootEnabled = false;
		}
		else if(difficulty == 1)
		{
			shootEnabled = true;
		}
		else if(difficulty == 2)
		{
			shootEnabled = true;
			maxDelay = 7.0f;
		}
	}
	//base stats
	public float health = 2;
	public float speed = 18.0f;

	//misc
	public int numGibs = 10;
	public float gibRadius = 0.1f;
	public float gibExplosionForce = 550.0f; 

	//stuff to get the enemy on screen
	public int direction = 1;
	public float crawlSpeed = 3.0f;
	public float dropDistance = 3.0f;
	public float acceleration = 0.0f;
	
	//shootin n stuff
	public bool shootEnabled = false;
	public float shootDelay; 
	public float minDelay = 2.0f;
	public float maxDelay = 10.0f;
	public Vector3 projSpeed = new Vector3(0, 18.0f, 0);
	public string projPathName = "EnemyRedLaser";
	
	public float shotPadding = -0.5f;

	public bool onScreen = false;
}


