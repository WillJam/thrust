using System;
using UnityEngine;

//raider
public class BasicEnemyController :EnemyController
{
	public new  BasicEnemyModel _model;
	public new BasicEnemyView _view;
	public BasicEnemyController (string name, BasicEnemyModel model, GibPool pool) :base(name, model, pool) 
	{
		_model = model;
	}
	
	public override void Build(Vector3 pos)
	{
		base.Build(pos);
		_model.shootDelay = UnityEngine.Random.Range(_model.minDelay, _model.maxDelay);
		
		_view = gameObject.AddComponent<BasicEnemyView>();
		_view.Build(_model);

		_view.Events.Shoot += HandleShoot;
		Events.Hit += HandleHit;
		Events.Dead += HandleDeath;
		texture.Events.BecameInvisible += HandleBecameInvisible;
	}

	void HandleBecameInvisible (object sender, EventArgs e)
	{
		if(_model.onScreen)
		{
			enemyName = "";
			Events.Dead -= HandleDeath;
			Events.OnDeath(sender, this);
			Destroy(_view);
		}
	}

	void HandleShoot (object sender, EventArgs e)
	{
		_model.shootDelay = UnityEngine.Random.Range(_model.minDelay, _model.maxDelay);
		var pM = new ProjectileModel(_model.projSpeed, -1, _model.projPathName, Vector3.zero);
		var pC = new ProjectileController("EvilLaser", pM, _pool);
		var pos = gameObject.transform.position;
		pos.y += _model.shotPadding;
		pC.Build(pos);
	}

	void HandleDeath (object sender, EventArgs<EnemyController> e)
	{
		texture.Events.BecameInvisible -= HandleBecameInvisible;
		var explo = new ExplosionController();
		explo.Build(gameObject.transform.position, _model.numGibs, _model.gibRadius, _model.gibExplosionForce, _pool);
		Destroy(_view);
	}

	void HandleHit (object sender, EventArgs<GameObject> Hit)
	{
		_model.health -= CalculateDamage(Hit.Value.tag); 
		if(_model.health <= 0)
		{
			Events.OnDeath(sender, this);
		}
	}
}

