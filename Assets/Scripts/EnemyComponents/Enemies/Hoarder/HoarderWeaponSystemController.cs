// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using System;
using UnityEngine;
public class HoarderWeaponSystemController : MonoBehaviour
{
	GameObject ship; 


	WeaponSystemsModel wsm;
	WeaponController.WeaponState weaponState;
	float pauseTime = 1.0f; 
	bool started = false;
	//the time the hoarder stays on screen after stealing the power up 
	float escapeTime = 15.0f;
	GibPool _pool;
	Transform _player;

	public HoarderEvents Events = new HoarderEvents();

	public void Build(GameObject parent,Transform player, WeaponController.WeaponState state, GibPool pool)
	{
		ship = parent;
		weaponState = state;
		_pool = pool;
		_player = player;
	}
	
	void Update()
	{
		if(pauseTime > 0)
		{
			pauseTime -= Time.deltaTime;
		}
		else if(!started)
		{

			StartSequence();
			started = true;
		}

		if(started && escapeTime > 0)
		{
			escapeTime -= Time.deltaTime;
		}
		else if(started)
		{
			Events.OnReadyToEscape();
		}
	}

	void StartSequence()
	{
		Debug.Log("started:" + weaponState);
		if(weaponState == WeaponController.WeaponState.Lasers)
		{
			//do lasers this should really never happen but just in case it does!
			wsm = new WeaponSystemsModel(-1.0f, "EnemyRedLaser", 1.0f, -1, false);
			var laser = ship.AddComponent<HoarderLaser>();
			laser.Build(wsm, _pool);
		}
		else if (weaponState == WeaponController.WeaponState.TriBeam)
		{
			//do tri beam 
			wsm = new WeaponSystemsModel(-1.0f, "EnemyRedLaser", 1.0f, -1, false);
			var tri = ship.AddComponent<HoarderTri>();
			tri.Build(wsm, _pool);
		}
		else if(weaponState == WeaponController.WeaponState.Beam)
		{
			wsm = new WeaponSystemsModel(-1.0f, "HoarderBeam", 2.0f, -1, false);
			var beam = ship.AddComponent<HoarderBeam>();
			beam.Build(wsm, _pool);
		}
		else if(weaponState == WeaponController.WeaponState.Clipper)
		{
			wsm = new WeaponSystemsModel(-1.0f, "HoarderClip", 2.0f, -1, false);
			var clip = ship.AddComponent<HoarderClipper>();
			clip.Build(wsm, _pool);
		}
		else if(weaponState == WeaponController.WeaponState.MachineGun)
		{
			wsm = new WeaponSystemsModel(-1.0f, "MachineGun", 2.0f, -1, false);
			var clip = ship.AddComponent<HoarderMachineGun>();
			clip.Build(wsm, _pool);
		}
		else if(weaponState == WeaponController.WeaponState.SeekerShot)
		{
			wsm = new WeaponSystemsModel(-1.0f, "SeekerShot", 2.0f, -1, false);
			var seeker = ship.AddComponent<HoarderSeekerShot>();
			seeker.Build(wsm, _player,  _pool);
		}
	}
}


