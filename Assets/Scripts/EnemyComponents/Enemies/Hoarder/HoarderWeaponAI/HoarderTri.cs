using System;
using UnityEngine;

public class HoarderTri : MonoBehaviour
{

	float fireRate = 0.5f;
	WeaponSystems weaponSystems;
	public void Build(WeaponSystemsModel wsm, GibPool pool)
	{
		weaponSystems = gameObject.AddComponent<WeaponSystems>();
		weaponSystems.BuildWeaponSystems(gameObject, wsm, pool);
	}
	void Update()
	{
		if(fireRate > 0)
		{
			fireRate -= Time.deltaTime;
		}
		else
		{
			weaponSystems.FireTri();
			fireRate = 0.5f;
		}
	}
}
