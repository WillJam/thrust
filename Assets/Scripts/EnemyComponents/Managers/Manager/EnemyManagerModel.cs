using System;
using UnityEngine;

public class EnemyManagerModel
{
	public float WaveTimeDelay; 
	public int difficulty = 0;
	public ShipController Player { get; set;}

	public AudioClip destroyedExplosion = (AudioClip) Resources.Load("Sounds/Explosions/explosion_distant_001");
	public float midExplosion = 0.15f;
	public float bigExplosion = 0.35f;
	public float smallExplosion = 0.1f;


	
	public EnemyManagerModel(ShipController player)
	{
		Player = player;	
	}
	
}


