using System;

public class EnemyManagerEvents
{
	public event EventHandler<EventArgs> WaveDefeated;
	public void OnWaveDefeated()
	{
		var handler = WaveDefeated;
		if (handler != null) handler(this, EventArgs.Empty);
	}
	
	public event EventHandler<EventArgs> WaveTimerExpired;
	public void OnWaveTimerExpired()
	{
		var handler = WaveTimerExpired;
		if (handler != null) handler(this, EventArgs.Empty);
	}

	public event EventHandler<EventArgs<int>> UpdateScore;
	public void OnUpdateScore(int score)
	{
		var handler = UpdateScore;
		if(handler != null) handler(this, new EventArgs<int>(score));
	}
}

