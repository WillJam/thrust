using System.Collections.Generic;
using UnityEngine;


public class EnemyManagerController
{
	GameObject gameObject; 
	string _title; 
	
	EnemyManagerModel _model;
	
	EnemyFactory enemyFactory;
	GibPool _pool;
	bool restart = false;

	public EnemyManagerEvents Events = new EnemyManagerEvents();
	
	//number if enemies enemyList.Count
	List<EnemyController> enemyList; 

	SoundController sound;

	public EnemyManagerController (string name, EnemyManagerModel model, GibPool pool)
	{
		_title = name;
		_model = model;
		_pool = pool;
	}
	

	public void Build() 
	{
		restart = false;
		gameObject = new GameObject(_title);

		sound = new SoundController();
		GameObject child;
		child = new GameObject("enemysound"); child.transform.parent = gameObject.transform; sound.Build(child, false,false);

		enemyList = new List<EnemyController>();
		enemyFactory = gameObject.AddComponent<EnemyFactory>();
		enemyFactory.Build(_model.Player, _pool);
		enemyFactory.Events.WaveBuilt += HandleWaveBuilt;
	}

	void HandleWaveBuilt (object sender, System.EventArgs<List<EnemyController>> e)
	{
		var waveList = e.Value;

		foreach(var w in waveList)
		{
			enemyList.Add (w);
			w.Events.Dead += HandleEnemyDeath;
		}
		//Debug.Log("Total Wave size" + enemyList.Count);

	}
	
	public void Update()
	{
		if(_model.WaveTimeDelay >= 0)
		{
			_model.WaveTimeDelay -= Time.deltaTime;
		}
		else
		{
			Events.OnWaveTimerExpired();
		}

	}
	
	// Put enemy Builds into an enemy factory class
	//Build Basic enemy type
	public void BuildWave(string type, Vector3 startPosition, int x, int y, int xHeight, int yHeight, int difficulty, int time)
	{
		enemyFactory.BuildWave(type, startPosition, x, y, xHeight, yHeight, difficulty, time);

	}

	public void KillAll()
	{
		restart = true;
		foreach(var w in enemyList)
		{
			w.Remove();
		}
	}
	
	void HandleEnemyDeath (object sender, System.EventArgs<EnemyController> e)
	{
		CalculateScore(e.Value.enemyName);
		e.Value.Events.Dead -= HandleEnemyDeath;
		enemyList.Remove(e.Value);
		enemyList.TrimExcess();
		//Debug.Log ("DEATH " + enemyList.Count);


		//Debug.Log ("Enemy: " + e.Value.enemyName + " Num: " + enemyList.Count);
		if(enemyList.Count == 0)
		{
			Events.OnWaveDefeated();
		}
	}
	//add more build methods for different enemy types below
	
	//temp enableweaposn
	void CalculateScore(string name)
	{
		int score;
		if(sound.src != null)
		{

			if(name.Equals("Basic"))
			{
				score = 100;
				sound.SetVolume(_model.midExplosion);
			}
			else if (name.Equals("Blocker"))
			{
				score = 250;
				sound.SetVolume(_model.midExplosion);
			}
			else if (name.Equals("Hoarder"))
			{
				score = 500;
				sound.SetVolume(_model.bigExplosion);
			}
			else if (name.Equals("Hunter"))
			{
				score = 250;
				sound.SetVolume(_model.midExplosion);
			}
			else if (name.Equals("Stalker"))
			{
				score = 500;
				sound.SetVolume(_model.bigExplosion);
			}
			else if (name.Equals("Swarmer"))
			{
				score = 250;	
				sound.SetVolume(_model.bigExplosion);
			}
			else if (name.Equals("Mini"))
			{
				score = 10;
				sound.SetVolume(_model.smallExplosion);
			}
			else
			{
				score = 0;
				sound.SetVolume(0);
			}

			sound.RandomizePitch (0.5f, 1.25f);
			sound.PlayShot(_model.destroyedExplosion);

			
			Events.OnUpdateScore(score);
		}
	}

	public void Destroy()
	{
		GameObject.Destroy(gameObject);
	}


	public void SetPhaseTime(float waveTime)
	{
		_model.WaveTimeDelay = waveTime;
	}
}


