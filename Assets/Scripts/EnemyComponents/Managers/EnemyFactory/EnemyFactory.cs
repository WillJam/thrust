using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public class EnemyFactory :MonoBehaviour
{
	//put all of the enemy build methods in here
	ShipController ShipObject {get; set;}
	GibPool _pool;


	public EnemyFactoryEvents Events = new EnemyFactoryEvents();
	public void Build(ShipController shipObject, GibPool pool)
	{
		_pool = pool;
		ShipObject = shipObject;
	}


	public IEnumerator BuildWaveWait(string type, Vector3 startPosition, int x, int y, int xHeight, int yHeight, int difficulty, float time)
	{
		 
		yield return new WaitForSeconds(time);
		List<EnemyController> waveList = new List<EnemyController>();
		if(type.Equals("Swarmer"))
		{
			waveList = BuildSwarmerWave(startPosition, x, y, xHeight, yHeight, difficulty);
		}
		else
		{
			Vector3 pos = startPosition; 
			for(int i = 0; i < y; i++){
				pos.y += yHeight;
				pos.x = startPosition.x;
				for(int j = 0; j < x; j++){
					pos.x += xHeight;
					var enemyControl = GetControllerType(type, GetModelType(type, difficulty));
					enemyControl.Build(pos);
					waveList.Add(enemyControl);
				}
			}
		}
		//Debug.Log ("Wave Count" + waveList.Count);
		Events.OnWaveBuilt(waveList);
	}

	public void BuildWave(string type, Vector3 startPosition, int x, int y, int xHeight, int yHeight, int difficulty, int time)
	{
		StartCoroutine(BuildWaveWait(type, startPosition, x, y, xHeight, yHeight, difficulty, time));
	}

	List<EnemyController> BuildSwarmerWave (Vector3 startPosition, int x, int y, int xHeight, int yHeight, int difficulty)
	{
		List<EnemyController> waveList = new List<EnemyController>();
		int swarmerPosX = (int)Random.Range(1, x - 1);
		int swarmerPosY = (int)Random.Range(1, y - 1);
		Vector3 pos = startPosition; 
		for(int i = 0; i < y; i++){
			pos.y += yHeight;
			pos.x = startPosition.x;
			for(int j = 0; j < x; j++){
				pos.x += xHeight;
				EnemyController enemyControl;
				if(i == swarmerPosY && j == swarmerPosX)
				{
					enemyControl = GetControllerType("Swarmer", GetModelType("Swarmer", difficulty));
				}
				else
				{
					enemyControl = GetControllerType("Basic", GetModelType("Basic", difficulty));
				}

				enemyControl.Build(pos);
				waveList.Add(enemyControl);
			}
		}
		return waveList;
	}
	
	EnemyModel GetModelType(string type, int difficulty)
	{
		if(type.Equals ("Basic"))
		{
			return new BasicEnemyModel(difficulty);
		}
		else if(type.Equals ("Puller"))
		{
			return new PullerModel(difficulty, ShipObject);
		}
		else if(type.Equals("Blocker"))
		{
			return new BlockerModel(difficulty, ShipObject);
		}
		else if(type.Equals("Swarmer"))
		{
			return new SwarmerModel(difficulty, ShipObject);
		}
		else if(type.Equals("Hunter"))
		{
			return new HunterModel(difficulty, ShipObject);
		}
		else if(type.Equals("Hoarder"))
		{
			return new HoarderModel(difficulty, ShipObject);
		} 
		else if(type.Equals("Stalker"))
		{
			return new StalkerModel(difficulty);
		} 
		else
		{
			Debug.LogError("Something went wrong");
			return null;
		}
	}
	
	EnemyController GetControllerType(string type, EnemyModel model)
	{
		if(type.Equals("Basic"))
		{
			return new BasicEnemyController(type, (BasicEnemyModel) model, _pool);
		}
		else if(type.Equals("Puller"))
		{
			return new PullerController(type, (PullerModel) model, _pool);
		}
		else if(type.Equals("Blocker"))
		{
			return new BlockerController(type, (BlockerModel) model, _pool);
		}
		else if(type.Equals("Swarmer"))
		{
			return new SwarmerController(type, (SwarmerModel) model, _pool);
		}
		else if(type.Equals("Hunter"))
		{
			return new HunterController(type, (HunterModel) model, _pool);
		}
		else if(type.Equals("Hoarder"))
		{
			return new HoarderController(type, (HoarderModel) model, _pool);
		} 
		else if(type.Equals("Stalker"))
		{
			return new StalkerController(type, (StalkerModel) model, _pool, ShipObject);
		} 
		else 
		{			
			Debug.LogError("Something went wrong");
			return null;
		}

	}
}


