using System;
using System.Collections.Generic;
using UnityEngine;
public class ShipModel
{
	public float shipSpeed = 40.0f;
	public float shipRotation = 25.0f;
	public string defaultShipPathName = "Ship/";
	public string defaultTexturePathName = "Player_ship 2"; 

	//base stats
	public float health  = 15.0f;
	public float shields = 0;
	public float power = 100.0f;
	public float baseRegen = 7.0f;
	public float stallRegen = 15.0f;

	public bool controls = false;

	public bool stalling;

	public bool tutorial;
	//
	public float powerRechargeRate = 6.0f;

	public bool cooldown = false;
	//weapon power usages
	public float laserPower = 1.0f;
	public float triBeamPower = 3.0f;
	public float oBeamPower = 12.5f;
	public float clipBeamPower = 23.0f;
	public float machineGunPower = 1.0f;
	public float seekerShotPower = 5.0f;
	
	public WeaponController.WeaponState weaponAtQ {get; set;}
	public WeaponController.WeaponState weaponAtW {get; set;}
	public WeaponController.WeaponState weaponAtE {get; set;}

	public bool AssigningPowerUp = false;
	public WeaponController.WeaponState WeaponAssigned; 
	public WeaponController.WeaponState CurrentWeapon;
	public bool UtilityGet = false; 


	public AudioClip hit = (AudioClip) Resources.Load("Sounds/Explosions/bomb explosion 2");
	public float hitVolume = 0.05f;

	public AudioClip charging = (AudioClip) Resources.Load("Sounds/Herbie/charging");
	public float chargingVolume = 0.3f;

	public AudioClip click = (AudioClip) Resources.Load("Sounds/Herbie/button click_wep switch");
	public float clickVolume = 0.5f;



	//HUD stuff
	public bool HUD;
	public bool YOUWON; 
	

	public string shipTexturePathName{ get; set;} 
	public ShipModel(bool tut)
	{
		tutorial = tut;
		shipTexturePathName = defaultTexturePathName;

	}

	public Texture HudTexture = (Texture) Resources.Load("Textures/HUD/HUD");
	public readonly float HUDAlpha = 0.6f;

}


