using System;
using UnityEngine; 


public class ShipEvents
{
	public event EventHandler<EventArgs<float>> MoveHerbie;
	public void OnMoveHerbie(GameObject go, float axis)
	{
		var handler = MoveHerbie;
        if (handler != null) handler(this, new EventArgs<float>(axis));
	}

	public event EventHandler<EventArgs<bool>> Stall;
	public void OnStall(GameObject go, bool isStalling)
	{
		var handler = Stall;
		if (handler != null) handler(this, new EventArgs<bool>(isStalling));
	}
	
	public event EventHandler<EventArgs> Death;
	public void OnDeath()
	{
		var handler = Death;
		if(handler != null)  handler(this, EventArgs.Empty);
	}

	public event EventHandler<EventArgs> UpdatePower;
	public void OnUpdatePower()
	{
		var handler = UpdatePower;
		if(handler != null) handler(this, EventArgs.Empty);
	}
}

