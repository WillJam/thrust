using System;

using UnityEngine;

public class ShipView : MonoBehaviour
{
	
	public ShipEvents Events = new ShipEvents();
	public bool glow = false;
	float glowSpeed = 5.5f;
	public Light glowLight;
	float glowMoveSpeed = 13.0f;
	public bool gameOver = false;
	
	public ShipView ()
	{

	}
	
	public void BuildView(Vector3 pos)
	{
		gameObject.transform.position = pos;
	}
	
	void Update()
	{
		if(!gameOver)
		{
			if(Input.GetKey("s"))
			{
				Events.OnStall(gameObject, true);
			}
			else
			{
				Events.OnStall(gameObject, false);
			}
			
			float axis = Input.GetAxis("Horizontal"); 
			
			if(axis != 0)
			{
				Events.OnMoveHerbie(gameObject, axis); 
			} else
			{
				//generalize rotate speeds *** ** * * * * * * * ******* 
				UpdateRotation(Quaternion.identity, 10.0f);
			}
			
			UpdateGlow();
			UpdatePower();
		}
	}
	
	public void UpdatePosition(Vector3 newPosition)
	{
		gameObject.transform.position = newPosition;
	}
	
	public void UpdateRotation(Quaternion target, float smooth)
	{
		transform.rotation = Quaternion.Slerp(transform.rotation, target,
	                               Time.deltaTime * smooth);
	}

	void UpdatePower()
	{
		Events.OnUpdatePower();
	}

	public void UpdateGlow()
	{
		float targetValue; 
		var newPos = glowLight.transform.localPosition;
		if(glow)
		{
			targetValue = 10;
			newPos.y -= glowMoveSpeed * Time.deltaTime;
		}
		else
		{
			targetValue = 0;
		}
		glowLight.range = iTween.FloatUpdate(glowLight.range, targetValue, glowSpeed);

		if(glowLight.range >= 9.0)
		{
			glow = false;
		}
		if(glowLight.range <= 0.5f)
		{
			newPos.y = 7.5f;
		}
		glowLight.transform.localPosition = newPos;
	}
	
	public void Destroy()
	{
		gameObject.SetActive(false);
	}
}

