// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using System;
using UnityEngine;
public class PowerBarController
{

	GameObject gameObject;
	SpriteController bar;

	PowerBarModel _model;
	PowerBarView _view;

	Vector3 _localPosition;
	public PowerBarController (PowerBarModel model)
	{
		_model = model;
	}

	public void Build(GameObject parent, Vector3 localPosition, Vector3 localScale)
	{
		gameObject = new GameObject("PowerBarControl");
		gameObject.transform.parent = parent.transform;
		gameObject.transform.localPosition = new Vector3(1000, 1000, -50);
		_localPosition = localPosition;


		bar = new SpriteController(_model.powerBarPath, "PowerBar");
		bar.Build(gameObject, _model.powerBarCentering);  

		gameObject.transform.localScale = localScale;

		_view = gameObject.AddComponent<PowerBarView>();
		_view.Build(localScale, bar.GetSpriteRenderer());
	}

	public void UpdatePower(float power)
	{
		_view.power = power;
	}

	public void Init()
	{
		gameObject.transform.localPosition = _localPosition;
		_view.Init();
	}

	public void Alpha(float t, float a)
	{
		iTween.FadeTo(gameObject, a, t);
	}
}


