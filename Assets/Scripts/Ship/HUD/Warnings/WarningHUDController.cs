// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using System;
using UnityEngine;
public class WarningHUDController
{

	public GameObject gameObject;
	SpriteController hullEx;
	SpriteController powerEx;
	SpriteController hullType;
	SpriteController powerType;

	WarningHUDModel _model;
	public WarningHUDController ()
	{
		_model = new WarningHUDModel();
	}

	public void Build(GameObject parent)
	{
		gameObject = new GameObject("WarningPanel");
		gameObject.transform.parent = parent.transform;

		gameObject.transform.localPosition = new Vector3(10000, 10000, 50);

		hullEx = new SpriteController("Warnings/hullEx", "hullEx");
		powerEx = new SpriteController("Warnings/powerEx", "powerEx");
		hullType = new SpriteController("Warnings/hullType", "hullType");
		powerType = new SpriteController("Warnings/powerType", "powerType");

		hullEx.Build(gameObject, _model.hullExPos);
		powerEx.Build(gameObject, _model.powerExPos);
		hullType.Build(gameObject, _model.hullTypePos);
		powerType.Build(gameObject, _model.powerTypePos);


	}

	public void Init(Vector3 localPosition)
	{
		gameObject.transform.localPosition = localPosition;
	}

	public void Alpha(float t, float a)
	{
		iTween.FadeTo(gameObject,a,t);
		HideHullCrit();
	}

	public void ShowHullCrit()
	{
		hullEx.TweenAlpha(1,0);
		hullType.TweenAlpha(1,0);
		hullEx.Flash();
		hullType.Flash();
	}
	
	public void HideHullCrit()
	{
		hullEx.TweenAlpha(0,0);
		hullType.TweenAlpha(0,0);
	}
	
	public void ShowPowerCrit()
	{
		powerEx.TweenAlpha(1,0);
		powerType.TweenAlpha(1,0);
		powerEx.Flash();
		powerType.Flash();
	}
	
	public void HidePowerCrit()
	{
		powerEx.TweenAlpha(0,0);
		powerType.TweenAlpha(0,0);
	}


	public void Destroy()
	{

	}

}


