// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using System;
using UnityEngine;
public class TutorialController
{

	GameObject gameObject;

	SpriteController _moveAndShoot;
	SpriteController _defendYourself;
	SpriteController _chargePower;

	TutorialModel _model;
	TutorialView _view;

	public TutorialController ()
	{
		_model = new TutorialModel();
	}

	public void Build(GameObject parent, Vector3 localPosition)
	{
		gameObject = new GameObject("TutorialController");
		gameObject.transform.parent = parent.transform;
		gameObject.transform.localPosition = new Vector3(1000, 10000, -50);


		_moveAndShoot = new SpriteController("Tutorials/MoveAndShoot", "moveandshoot");
		_defendYourself = new SpriteController("Tutorials/Defend Yourself", "defendyourself");
		_chargePower = new SpriteController("Tutorials/ChargePower", "chargepower");

		_view = gameObject.AddComponent<TutorialView>();


		_view.Events.ShowDefend += HandleShowDefend;
		_view.Events.DoneWithMovement += HandleDoneWithMovement;

		_moveAndShoot.Build(gameObject, _model.moveAndShootPos);
		_defendYourself.Build(gameObject, _model.defendPos);
		_defendYourself.SetActive(false);
		_chargePower.Build(gameObject, _model.chargePos);
		_chargePower.SetActive(false);
		Alpha(0,0);
	}

	public void PowerTutorial()
	{
		_chargePower.SetActive(true);
		_chargePower.TweenAlpha(1, 0.5f);
	}

	public void HidePowerTutorial()
	{
		_chargePower.TweenAlpha(0, 0.5f);
	}

	void HandleDoneWithMovement (object sender, EventArgs e)
	{
		_moveAndShoot.TweenAlpha(0,0.5f);
		_defendYourself.TweenAlpha(0, 0.5f);
	}

	void HandleShowDefend (object sender, EventArgs e)
	{
		_defendYourself.TweenAlpha(0,0);
		_defendYourself.SetActive(true);
		_defendYourself.TweenAlpha(1, 0.5f);
	}

	public void Init()
	{

		gameObject.transform.localPosition = Vector3.zero;
		_view.StartBasicTutorialSequence();
	}

	public void Alpha(float t, float a)
	{
		iTween.FadeTo(gameObject, a, t);
		_moveAndShoot.TweenAlpha(a,t);
		_defendYourself.TweenAlpha(a,t);
		_chargePower.TweenAlpha(a,t);
	}

}



