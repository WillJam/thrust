
using System;
using UnityEngine;
using System.Collections.Generic;
public class PowerUpDisplayModel
{

	public string assignWeapon = "AssignWep";
	public Dictionary<string, SpriteController> powerUpDisplays = new Dictionary<string, SpriteController>();
	public Vector3 powerUpDisplayPosition = new Vector3(0, 1, -15);
	public Vector3 powerUpAssignPosition = new Vector3(0, 0, -15);

	public PowerUpDisplayModel ()
	{
		BuildPowerUpDisplays();
	}

	void BuildPowerUpDisplays()
	{


		//add healthpak 
		powerUpDisplays.Add(WeaponController.WeaponState.Beam.ToString(), new SpriteController("PowerUp/OBeamGet", "Beam"));
		powerUpDisplays.Add(WeaponController.WeaponState.Clipper.ToString(), new SpriteController("PowerUp/ClipperGet", "Clipper"));
		powerUpDisplays.Add(WeaponController.WeaponState.MachineGun.ToString(), new SpriteController("PowerUp/MachineGunGet", "Machine"));
		powerUpDisplays.Add(WeaponController.WeaponState.SeekerShot.ToString(), new SpriteController("PowerUp/SeekerShotGet", "SeekerShot"));
		powerUpDisplays.Add(WeaponController.WeaponState.TriBeam.ToString(), new SpriteController("PowerUp/TriBeamGet", "TriBeam"));

	}
}


