using System;
using UnityEngine;


//Weapon Controller specific to herbie
public class WeaponController
{
	GameObject Ship;
	WeaponView _view;
	WeaponSystems wsControl;

	private bool weaponsEnabled = true;
	
	public WeaponState ws;

	public WeaponSystemsEvents Events = new WeaponSystemsEvents();
	
	public WeaponController ()
	{
		ws = WeaponState.Lasers;
	}
	
	public enum WeaponState
	{
		Lasers,
		TriBeam,
		Beam,
		Clipper,
		MachineGun,
		SeekerShot,
		Healthpak,
		Nova
	}
	
	public void Build(GameObject parent, GibPool pool)
	{
		Ship = parent;
		
		var wsModel = new WeaponSystemsModel();
		wsControl = Ship.AddComponent<WeaponSystems>();
		wsControl.BuildWeaponSystems(Ship, wsModel, pool);
		
		_view = Ship.AddComponent<WeaponView>();
		
		_view.Events.SpaceDown += HandleSpaceDown;
		_view.Events.SpaceHeld += HandleSpaceHeld;
		_view.Events.SpaceUp += HandleSpaceUp;

		wsControl.Events.UpdatePower += HandleUpdatePower;
		
	}

	void HandleSpaceUp (object sender, EventArgs e)
	{
		wsControl.SpaceUp(ws);
	}

	void HandleUpdatePower (object sender, EventArgs<WeaponState> e)
	{
		Events.OnUpdatePower(e.Value);
	}

	void HandleSpaceHeld (object sender, EventArgs<bool> e)
	{
		if(weaponsEnabled && e.Value)
		{
			wsControl.FireWeapon (ws, true);
		}
	}

	public void HideMuzzleFlashes()
	{
		wsControl.HideMuzzleFlashes();
	}

	void HandleSpaceDown (object sender, EventArgs e)
	{
		if(weaponsEnabled)
		{
			wsControl.FireWeapon(ws, false);
		}
	}

	public void Enable()
	{
		weaponsEnabled = true;
		wsControl.Enable();
	}

	public void Disable()
	{
		HandleSpaceUp(this, EventArgs.Empty);
		weaponsEnabled = false;
		wsControl.Disable(ws);
	}

	public void ChangePowerup()
	{
		wsControl.ChangePowerup(ws);
	}

	public SeekerEvents GetSeekerEvents()
	{
		return wsControl.GetSeekerEvents();
	}
	
}

 