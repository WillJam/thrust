using System;
using UnityEngine;

public class WeaponSystemsModel
{
	public float projectileStartPositionY = 2f;
	
	public float DIRECTION = 1.0f; //1 for herbie, -1 for enemy i.e. hoarder or other scary ships
	public string laserName = "Laser";
	public Vector3 laserSpeed = new Vector3(0, 28.0f, 0);
	public Vector3 triSpeedLeft = new Vector3(-8.0f, 28.0f, 0);
	public Vector3 triSpeedRight = new Vector3(8.0f, 28.0f, 0);
	public Vector3 triRotRight = new Vector3(0, 0, -12);
	public Vector3 triRotLeft = new Vector3(0, 0, 12);

	public string laserPathName = "LaserTexture";
	public float laserFireRate = 0.3f; 
	public float laserDelay;

	public float beamPowerRate = 1.0f;
	public float beamPowerDelay;

	public string machineBulletPath = "MachineBullet";
	public float machineGunFireRate = 0.04f;
	public Vector3 muzzlePosition = new Vector3(1,3.2f,0);
	public Vector3 leftMuzzlePosition = new Vector3(-1, 3.2f ,0);
	public float machineGunDelay;
	public Vector3 machineBulletSpeed = new Vector3(0, 50.0f, 0);
	public Quaternion muzzleFlashRotation = Quaternion.identity;
	public Vector3 muzzlePaddingLeft = new Vector3(1,3.2f,0);
	public Vector3 muzzlePaddingRight = new Vector3(-1, 3.2f ,0);


	public bool left;

	public string beamPath = "Projectiles/BeamWeapon";
	public Vector3 beamPosition = Vector3.zero;


	public bool playerSystem = true;
	public int invertBeams = 1;

	public Vector3 clipPos = new Vector3(0, 2.5f);
	//beam


	public WeaponSystemsModel()
	{
		laserDelay = laserFireRate;
		machineGunDelay = machineGunFireRate;
		beamPowerDelay = 0;
	}

	public WeaponSystemsModel(float direction, string pathName, float projStartY, int InvertBeams, bool PlayerSystem)
	{
		DIRECTION = direction;
		laserPathName = pathName;
		projectileStartPositionY = projStartY;
		invertBeams = InvertBeams;
		playerSystem = PlayerSystem;
		if(!playerSystem)
		{
			beamPath = "Projectiles/HoarderBeam";
			beamPosition = new Vector3(0, projStartY, 0);
		}

	}
	
}

