using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSystems : MonoBehaviour
{
	//this class controls powerup/weapon states
	GameObject Ship;
	WeaponSystemsModel _model;

	public WeaponSystemsEvents Events = new WeaponSystemsEvents();

	BeamController Beam;
	ClipperController Clipper;
	SeekerController Seeker;

	GibPool _pool;

	PlayerWeaponSoundController weaponSounds;

	bool enabled;

	List<SpriteController> muzzleflashes = new List<SpriteController>();
	SpriteController flash;

	public void BuildWeaponSystems(GameObject parent, WeaponSystemsModel model, GibPool pool)
	{
		Ship = parent; 
		_pool = pool;
		_model = model;

		weaponSounds = new PlayerWeaponSoundController();
		weaponSounds.Build(Ship);

		Beam = new BeamController("Beam", new BeamModel(_model.beamPath, _model.invertBeams, _model.playerSystem), pool);
		Beam.Build (Ship.transform, _model.beamPosition);

		Clipper = new ClipperController(new ClipperModel(_model.beamPath, _model.invertBeams, _model.playerSystem));
		Clipper.Build(Ship.transform, pool, _model.clipPos);

		Seeker = new SeekerController(new SeekerModel(), pool, _model.playerSystem);
		Seeker.Build(parent.transform);

		for(int i = 0; i < 4; i++)
		{
			SpriteController muzzleflash = new SpriteController(("Muzzle Flash/Muzzle Flash_" + (i + 1)), "mFlash_" + (i+ 1));
			muzzleflash.Build(Ship, _model.muzzlePosition, new Vector3(0.7f, 0.7f, 0), _model.muzzleFlashRotation);
			muzzleflashes.Add((SpriteController)muzzleflash);
			muzzleflash.SetActive(false);
			flash = muzzleflash;
		}
	}
	
	public void FireWeapon(WeaponController.WeaponState ws, bool spaceHeld)
	{
		if(enabled)
			{
			if(WeaponController.WeaponState.Lasers == ws || WeaponController.WeaponState.TriBeam == ws)
			{
				
				//space is held and delay has been accounted for 
				if (spaceHeld && _model.laserDelay <= 0 )
				{
					if(WeaponController.WeaponState.Lasers == ws)
					{
						FireLaser();
					}
					else if(WeaponController.WeaponState.TriBeam == ws)
					{
						FireTri();
					}
					_model.laserDelay = _model.laserFireRate;
				} 
				//space is held but delay isn't done yet
				else if (spaceHeld && _model.laserDelay > 0)
				{
					_model.laserDelay -= Time.deltaTime; 
				}
				
				//first shot
				if(!spaceHeld)
				{
					_model.laserDelay = _model.laserFireRate - 0.1f;
					if(WeaponController.WeaponState.Lasers == ws)
					{
						FireLaser();
					} 
					else if(WeaponController.WeaponState.TriBeam == ws)
					{
						FireTri();
					}
				}
			}

			if(WeaponController.WeaponState.MachineGun == ws)
			{
				if (spaceHeld && _model.machineGunDelay <= 0 )
				{
					FireMachineGun();
					_model.machineGunDelay = _model.machineGunFireRate;
				} 
				//space is held but delay isn't done yet
				else if (spaceHeld && _model.machineGunDelay > 0)
				{
					_model.machineGunDelay -= Time.deltaTime; 
				}
				
				//first shot
				if(!spaceHeld)
				{
					FireMachineGun();
					_model.machineGunDelay = _model.machineGunFireRate - 4.0f;
				}
			}

			if(WeaponController.WeaponState.Beam == ws)
			{
				FireBeam();
			}

			if(WeaponController.WeaponState.Clipper == ws)
			{
				FireClipper();
			}

			if(WeaponController.WeaponState.SeekerShot == ws)
			{
				StartSeekerBeam();
			}
		}
	}


	public void FireBeam()
	{
		Beam.StartBeam();
		if(_model.beamPowerDelay <= 0)
		{
			weaponSounds.BeamShot();
			_model.beamPowerDelay = _model.beamPowerRate;
			Events.OnUpdatePower(WeaponController.WeaponState.Beam);
		}
		else if(_model.beamPowerDelay > 0)
		{
			_model.beamPowerDelay -= Time.deltaTime;
		}
	}

	public void StopBeam()
	{
		_model.beamPowerDelay = 0;
		weaponSounds.StopBeamShot();
		Beam.StopBeam();
	}

	public void StartSeekerBeam()
	{
		Seeker.StartBeam();
	}

	public void StopSeekerBeam()
	{
		Seeker.StopBeam();
	}
	

	public void FireClipper() 
	{
		Clipper.StartBeams();
		if(_model.beamPowerDelay <= 0)
		{
			weaponSounds.BeamShot();
			_model.beamPowerDelay = _model.beamPowerRate;
			Events.OnUpdatePower(WeaponController.WeaponState.Clipper);
		}
		else if(_model.beamPowerDelay > 0)
		{
			_model.beamPowerDelay -= Time.deltaTime;
		}
	}
	
	public void StopClipper()
	{
		_model.beamPowerDelay = 0;
		weaponSounds.StopBeamShot();
		Clipper.StopBeams();
	}
	
	public void FireLaser()
	{
		weaponSounds.LaserOneShot();
		Events.OnUpdatePower(WeaponController.WeaponState.Lasers);
		ProjectileFactory(_model.laserName, _model.laserSpeed, _model.DIRECTION, _model.laserPathName, Vector3.zero);
	}
		
	public void FireTri()
	{	
		weaponSounds.TriBeamShot();
		Events.OnUpdatePower(WeaponController.WeaponState.TriBeam);
		ProjectileFactory(_model.laserName, _model.triSpeedRight, _model.DIRECTION, _model.laserPathName, _model.triRotRight);
		ProjectileFactory(_model.laserName, _model.triSpeedLeft, _model.DIRECTION, _model.laserPathName, _model.triRotLeft);
		ProjectileFactory(_model.laserName, _model.laserSpeed, _model.DIRECTION, _model.laserPathName, Vector3.zero);
	}

	public void FireMachineGun()
	{
		_model.left = !_model.left;
		int rand = Random.Range(0, muzzleflashes.Capacity);
		flash.SetActive(false);
		flash = muzzleflashes[rand];

		weaponSounds.PlayMachineGun();

		if(_model.left)
		{
			ProjectileFactory("MachineGunBullet", _model.machineBulletSpeed, _model.DIRECTION, _model.machineBulletPath, Vector3.zero, _model.muzzlePaddingLeft);
			//show left muzzle flash
			flash.Position(_model.leftMuzzlePosition);
			flash.SetActive(true);
			Events.OnUpdatePower(WeaponController.WeaponState.MachineGun);
		}
		else
		{
			ProjectileFactory("MachineGunBullet", _model.machineBulletSpeed, _model.DIRECTION, _model.machineBulletPath, Vector3.zero, _model.muzzlePaddingRight);
			//show right muzzle flash
			flash.Position(_model.muzzlePosition);
			flash.SetActive(true);
			Events.OnUpdatePower(WeaponController.WeaponState.MachineGun);
		}
	}
	
	void ProjectileFactory(string name, Vector3 speed, float direction, string path, Vector3 rotation)
	{
		
		ProjectileModel pModel = new ProjectileModel(speed, direction, path, rotation);
		ProjectileController newProjectile = new ProjectileController(name, pModel, _pool);
		var startPos = Ship.transform.position;
		startPos.y += _model.projectileStartPositionY;
		newProjectile.Build(startPos);
	}

	//path means pathto teture
	void ProjectileFactory(string name, Vector3 speed, float direction, string path, Vector3 rotation, Vector3 StartPosPadding)
	{
		
		ProjectileModel pModel = new ProjectileModel(speed, direction, path, rotation);
		ProjectileController newProjectile = new ProjectileController(name, pModel, _pool);
		var startPos = Ship.transform.position;
		startPos.y += _model.projectileStartPositionY;
		startPos += StartPosPadding;
		newProjectile.Build(startPos);
	}
	
	public void ChangePowerup(WeaponController.WeaponState ws)
	{
		SpaceUp(ws);
	}

	public void HideMuzzleFlashes()
	{
		foreach(SpriteController m in muzzleflashes) { /*Debug.Log("Musszle");*/Debug.Log("MACHINE!"); m.SetActive(false);}
	}

	public void SpaceUp(WeaponController.WeaponState ws)
	{

		HideMuzzleFlashes();
		weaponSounds.StopMachineGun();
		StopBeam();
		StopClipper();
		StopSeekerBeam();
	}

	public SeekerEvents GetSeekerEvents()
	{
		return Seeker.Events;
	}

	public void Enable()
	{
		enabled = true;
	}

	public void Disable(WeaponController.WeaponState ws)
	{
		SpaceUp(ws);
		enabled = false;
	}

}
	


