using System;
using UnityEngine;

public class WeaponEvents
{
	public event EventHandler<EventArgs> SpaceDown;
	public void OnSpaceDown(GameObject go)
	{
		var handler = SpaceDown;
		if (handler != null) handler(this, EventArgs.Empty);
	}
	
	public event EventHandler<EventArgs<bool>> SpaceHeld;
	public void OnSpaceHeld(GameObject go, bool state)
	{
		var handler = SpaceHeld;
		if (handler != null) handler(this, new EventArgs<bool>(state));
	}

	public event EventHandler<EventArgs> SpaceUp;
	public void OnSpaceUp(GameObject go)
	{
		var handler = SpaceUp;
		if (handler != null) handler(this, EventArgs.Empty);
	}
	
}

