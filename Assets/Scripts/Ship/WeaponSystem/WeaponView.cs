using System;
using UnityEngine;
using System.Collections;

public class WeaponView : MonoBehaviour
{
	public WeaponEvents Events = new WeaponEvents();
	public bool firstShot = true;


	void Start()
	{

	}

	public WeaponView ()	{
		
	}
	
	
	IEnumerator firstShotDelay()
	{
		yield return new WaitForSeconds(0.1f);
		firstShot = false;
	}
	
	void Update()
	{

		if(Input.GetKey("space"))
		{
			if(!firstShot)
			{
				Events.OnSpaceHeld(gameObject, true);
			}
		}
		
		if(Input.GetKeyDown("space"))
		{
			Events.OnSpaceDown(gameObject);
			StartCoroutine("firstShotDelay");
		}

		if(Input.GetKeyUp("space"))
		{
			Events.OnSpaceUp(gameObject);
		}

		/*
		if(Input.GetKeyUp("space"))
		{
			Events.OnSpaceHeld(gameObject, false);
			StopCoroutine("firstShotDelay");
			firstShot = true;
		}
		*/
	}
}

