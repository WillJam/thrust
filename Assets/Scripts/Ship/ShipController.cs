using UnityEngine;

public class ShipController
{
	public GameObject ShipObject;
	public string shipName; 
	public Collider collider; 

	private ShipModel _model; 
	private ShipView _view;
	
	private WeaponController weaponsController; 
	private Texture3DController textureControl;
	private PowerUpSystemController powerUpController;
	private HUDController HUDControl;
	private PowerUpDisplayController pUpDisplay;
	
	public ShipEvents Events = new ShipEvents();


	public SoundController sounds;
	public SoundController chargingSounds;

	public Light powerUpGlowLight;

	bool powercritflashed = false;

	bool firstStall = true;

	//I HATE THIS CLASS. not proud of this code. 
	public ShipController(string ShipName, ShipModel model)
	{
		shipName = ShipName;
		_model = model;
	}
	
	public void Build(Vector3 position, GibPool pool)
	{
		ShipObject = new GameObject(shipName);
		ShipObject.tag = "herbie";


		sounds = new SoundController();

		sounds.Build(ShipObject, false,false);


		chargingSounds = new SoundController();

		chargingSounds.Build(ShipObject, false,false);
		//build view
		_view = ShipObject.AddComponent<ShipView>();
		_view.BuildView(position);
		
		//build ship 3Dmodel 
		textureControl = new Texture3DController(_model.defaultShipPathName + _model.shipTexturePathName, shipName); 
		textureControl.Build(ShipObject);
		collider = textureControl.collider;

		//get light
		powerUpGlowLight = GameObject.FindGameObjectWithTag("light").GetComponent<Light>();
		powerUpGlowLight.enabled = false;
		_view.glowLight = powerUpGlowLight;
		
		//build weapons systems;
		weaponsController = new WeaponController();
		weaponsController.Build(ShipObject, pool);

		
		//build power up system
		//maybe have a model that is mutable!!! AYAY, that way we can start the player with some fancy weapons or something if we want
		var pUpModel = new PowerUpSystemModel();
		_model.weaponAtQ = pUpModel.weaponAtQ;
		_model.weaponAtW = pUpModel.weaponAtW;
		_model.weaponAtE = pUpModel.weaponAtE;
		powerUpController = new PowerUpSystemController(pUpModel);
		powerUpController.Build (ShipObject);

		//buildHUD
		HUDControl = new HUDController(_model);
		HUDControl.Build(ShipObject);
		HUDControl.Hide(0);

		//Build pUp display a display that is shown everytime you get a powerup. 
		pUpDisplay = new PowerUpDisplayController(new PowerUpDisplayModel());
		pUpDisplay.Build();
		pUpDisplay.Hide(0);

		//events


	}

	public void UpdateScore(int value)
	{
		HUDControl.UpdateScore(value);
	}

	public void InitShipEvents()
	{
		//events
		powerUpController.Events.WeaponSelect += HandleWeaponSelect;
		_view.Events.MoveHerbie += HandleMoveHerbie;
		_view.Events.Stall += HandleStall;


		_view.Events.UpdatePower += HandleUpdatePower;
		weaponsController.Events.UpdatePower += HandleWeaponPower;
		textureControl.Events.TriggerEnter += HandleHit;
		textureControl.Events.TriggerStay += HandleHit;

		weaponsController.GetSeekerEvents().LockOn += HandleLockOn;
	}

	public void RemoveEvents()
	{
		//for on death
		iTween.Stop (ShipObject);
		powerUpController.Events.WeaponSelect -= HandleWeaponSelect;
		_view.Events.MoveHerbie -= HandleMoveHerbie;
		_view.Events.Stall -= HandleStall;
		
		
		_view.Events.UpdatePower -= HandleUpdatePower;
		weaponsController.Events.UpdatePower -= HandleWeaponPower;
		textureControl.Events.TriggerEnter -= HandleHit;
		textureControl.Events.TriggerStay -= HandleHit;
		
		weaponsController.GetSeekerEvents().LockOn -= HandleLockOn;
	}

	void HandleLockOn (object sender, System.EventArgs e)
	{
		float newPower = _model.power;
		newPower -= _model.seekerShotPower;
		if(newPower < 0)
		{
			newPower = 0;
			weaponsController.Disable();
			_model.cooldown = true;
		}
		_model.power = newPower;
	}

	void HandleWeaponPower (object sender, System.EventArgs<WeaponController.WeaponState> e)
	{
		CalculateWeaponPowerUsage(e.Value);
	}

	void CalculateWeaponPowerUsage (WeaponController.WeaponState value)
	{
		float newPower = _model.power;
		if(value == WeaponController.WeaponState.Lasers)
		{
			newPower -= _model.laserPower;
		}
		else if(value == WeaponController.WeaponState.TriBeam)
		{
			newPower -= _model.triBeamPower;
		}
		else if(value == WeaponController.WeaponState.Beam)
		{
			newPower -= _model.oBeamPower;
		}
		else if(value == WeaponController.WeaponState.Clipper)
		{
			newPower -= _model.clipBeamPower;
		}
		else if(value == WeaponController.WeaponState.MachineGun)
		{
			newPower -= _model.machineGunPower;
		}

		if(newPower < 0)
		{
			newPower = 0;
			/*
			weaponsController.Disable();
			//Debug.Log ("weapons disabled");
			_model.cooldown = true;
			*/
			weaponsController.HideMuzzleFlashes();
			weaponsController.Disable();
			weaponsController.ws = WeaponController.WeaponState.Lasers;
			weaponsController.Enable();
			HUDControl.WeaponSelect(5);
		}

		_model.power = newPower;
	}

	void HandleUpdatePower (object sender, System.EventArgs e)
	{
		float newPower = _model.power + _model.powerRechargeRate * Time.deltaTime;
		//Debug.Log("new power " + newPower);
		if(newPower > 100)
		{
			newPower = 100;
		}
		if(newPower < 25)
		{
			if(!powercritflashed)
			{
				HUDControl.ShowPowerCrit();
				powercritflashed = true;
			}

		}
		else
		{
			HUDControl.HidePowerCrit();
			powercritflashed = false;
		}

		if(newPower > 15 && !_model.stalling && !_model.AssigningPowerUp)
		{
			weaponsController.Enable();
			_model.cooldown = false;
		}
		_model.power = newPower;
		HUDControl.UpdatePowerValue(_model.power);
		
	}

	void HandleStall (object sender, System.EventArgs<bool> e)
	{
		/*
		if(_model.AssigningPowerUp && e.Value)
		{
			weaponsController.Enable();
			Time.timeScale = 1.0f;
			pUpDisplay.Hide(1);
			_model.AssigningPowerUp = false;
		}
		else */
		if(e.Value)
		{
			//do stalling stuff
			if(firstStall)
			{
				chargingSounds.PlayShot(_model.click,_model.clickVolume);
				chargingSounds.PlayShot(_model.charging, _model.chargingVolume);
				firstStall = false;	
			}
			_model.stalling = e.Value;
			_model.powerRechargeRate = _model.stallRegen;
			weaponsController.Disable();
		}
		else
		{
			if(!firstStall)
			{
				Debug.Log("fade");
				chargingSounds.Stop();
				firstStall = true;
			}
			_model.stalling = e.Value;
			_model.powerRechargeRate = _model.baseRegen;
		}
	}

	public void ShowHUD(float t)
	{

		_model.HUD = true;
		HUDControl.Show(t);
	}
	

	void HandleHit (object sender, System.EventArgs<GameObject> e)
	{
		//poweruplayer


		if(e.Value.layer == 15)
		{
			PowerUpGet();
		}
		else //if hit by any hostile things!!!
		{
			sounds.PlayShot(_model.hit, _model.hitVolume);

			_model.health -= CalculateDamage(e.Value.tag);
			HUDControl.UpdateHealth((int)_model.health);
			if(_model.health <= 3)
			{
				HUDControl.ShowHullCrit();
			}
			if(_model.health <= 0)
			{
				HerbieDied();
			}
		}
	}

	void HerbieDied()
	{
		_view.gameOver = true;
		Debug.Log ("DEAD");
		HUDControl.UpdateHealth(0);
		pUpDisplay.Destroy();
		HUDControl.Destroy();
		textureControl.DestroyMe();
		_view.Destroy();
		Events.OnDeath();
	}
	

	void HandleWeaponSelect (object sender, System.EventArgs<WeaponController.WeaponState> e)
	{
		if(_model.AssigningPowerUp)
		{
			_model.AssigningPowerUp = false;
			int weaponAssignInt = powerUpController.AssignWeapon(_model.WeaponAssigned);
			HUDControl.UpdatePowerUpHUD(weaponAssignInt, _model.WeaponAssigned);
			UpdatePowerUpGUI();
			weaponsController.ws = _model.WeaponAssigned;
			_model.CurrentWeapon = _model.WeaponAssigned;
			weaponsController.Enable();
			Time.timeScale = 1f;
			weaponsController.ChangePowerup();

			//Hide power up display! 
			pUpDisplay.Hide(1);

		}
		else if(weaponsController.ws == e.Value)
		{
			//SEND AN EVENT TO HUD CONTROLLER deactivating the current tab
			weaponsController.ws = WeaponController.WeaponState.Lasers;
			_model.CurrentWeapon = WeaponController.WeaponState.Lasers;
			//Debug.Log("Weapon Deactivated ");
			HUDControl.WeaponSelect(0);
			weaponsController.ChangePowerup();
		}
		else
		{
			//Debug.Log("Weapon Selected: " + e.Value);
			weaponsController.ws = e.Value;
			_model.CurrentWeapon = e.Value;
			//Debug.Log (powerUpController.GetSelectedSlot());
			HUDControl.WeaponSelect(powerUpController.GetSelectedSlot());
			weaponsController.ChangePowerup();
		}

	}
	
	private void HandleMoveHerbie (object sender, System.EventArgs<float> e)
	{
		if(_model.controls)
		{
			var axis = e.Value;
			if(e.Value != 0){
				var newPos = ShipObject.transform.position;
				newPos.x += axis * _model.shipSpeed * Time.deltaTime;
				
				var tiltAroundY = -axis * _model.shipRotation;
				var newRotation = Quaternion.Euler (0, tiltAroundY, 0);
				// *********generalize smooth for rotation**************
				_view.UpdateRotation(newRotation, 15.0f);
				_view.UpdatePosition(newPos);
				//******** test to see if herbie is at the edge of the screen and handle dat shit if he is! ***********
			}
		}
	}

	void PowerUpGet()
	{
		//Debug.Log("PowerUpGET!!");
		bool healthpak = false;
		//do the glow thing
		powerUpGlowLight.enabled = true;
		_view.glow = true;

		//find the powerup so we can get it's key!
		GameObject pUp = GameObject.Find("powerup");
		_model.WeaponAssigned = pUp.GetComponent<PowerUpView>().PowerUpKey;
		Debug.Log("HEYYYYOOO : " + _model.WeaponAssigned);

		//translate the key into a weaponstate for the weapon controller


		if(_model.WeaponAssigned != WeaponController.WeaponState.Healthpak)
		{
			//slow down time and disable weapons to make sure the player assigns his power up!!!
			HUDControl.HideTutorials();
			Time.timeScale = 0.2f;
			weaponsController.Disable();
			_model.AssigningPowerUp = true;
			//show correct weapon to be assigned
			pUpDisplay.Show(_model.WeaponAssigned.ToString(), 0.1f);

		}
		else
		{
			HealthPakGet();
		}
		//destroy the thing jiu lee
		GameObject.Destroy(pUp);
	}

	public bool CheckPowerUpArray(WeaponController.WeaponState state)
	{
		//if player has powerup return true
	
		foreach (WeaponController.WeaponState x in GetPowerUpArray())
		{
			if(x == state)
			{
				return true;
			}
		}
		return false;
	}

	public void HealthPakGet()
	{
		_model.AssigningPowerUp=false;
		Time.timeScale = 1f;
		var newHealth = _model.health + 3;
		_model.health += 3;
		if(newHealth > 15)
		{
			_model.health = 15;
		}
		else
		{
			_model.health = newHealth;
		}
		HUDControl.UpdateHealth((int)_model.health);
		HUDControl.HideHullCrit();
	}

	public void RemovePowerUpAt(int index)
	{
		weaponsController.Disable();
		HUDControl.UpdatePowerUpHUD(index + 1, WeaponController.WeaponState.Lasers);
		powerUpController.RemovePowerUpAt(index);
		UpdatePowerUpGUI();
	}

	void UpdatePowerUpGUI()
	{
		var array = GetPowerUpArray();
		_model.weaponAtQ = array[0];
		_model.weaponAtW = array[1];
		_model.weaponAtE = array[2];
	}


	public WeaponController.WeaponState[] GetPowerUpArray()
	{
		return powerUpController.GetPowerUpArray();
	}

	public void WinCondition ()
	{
		_model.YOUWON = true;
	}

	public void SetControls(bool t)
	{
		_model.controls = t;
		if(t)
		{
			weaponsController.Enable();
		}
		else
		{
			weaponsController.Disable();
		}
	}



	float CalculateDamage(string projectileTag)
	{
		if(projectileTag.Equals("enemybaselaser"))
		{
			textureControl.Flash();
			return 1.0f;
		}
		else if(projectileTag.Equals("blockerbeam"))
		{
			textureControl.Flash();
			return 1f;
		}
		else if(projectileTag.Equals("hunterbullet"))
		{
			textureControl.Flash();
			return 3.0f;
		}
		else if(projectileTag.Equals("mini"))
		{			
			textureControl.Flash();
			return 1.0f;
		}
		else if(projectileTag.Equals("enemybullet"))
		{
			textureControl.Flash();
			return 1f;
		}
		else 
		{
			//harmless
			return 0;
		}
	}
}