using System;
using UnityEngine;
	
public class PowerUpSystemView : MonoBehaviour
{
	
	public PowerUpSystemEvents Events = new PowerUpSystemEvents();
	PowerUpSystemModel _model;
	


	public void BuildView(PowerUpSystemModel model)
	{
		_model = model;
	}


	void Update()
	{	
		if(Input.GetKeyDown("q"))
		{
			_model.CurrentWeaponSlot = 1;
			Events.OnWeaponSelect(this, _model.weaponAtQ);
		}

		if(Input.GetKeyDown("w"))
		{
			_model.CurrentWeaponSlot = 2;
			Events.OnWeaponSelect(this, _model.weaponAtW);
		}

		if(Input.GetKeyDown("e"))
		{
			_model.CurrentWeaponSlot = 3;
			Events.OnWeaponSelect(this, _model.weaponAtE);
		}
	}
}

