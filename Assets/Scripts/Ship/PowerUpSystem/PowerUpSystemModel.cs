using System.Collections.Generic;
using UnityEngine;

public class PowerUpSystemModel
{
	public PowerUpSystemModel ()
	{
		//TEST
		//END TEST
		//

		weaponAtE = WeaponController.WeaponState.SeekerShot;
		weaponAtQ = WeaponController.WeaponState.TriBeam;
		weaponAtW = WeaponController.WeaponState.MachineGun;

		weaponAtE = WeaponController.WeaponState.Beam;
		weaponAtQ = WeaponController.WeaponState.Clipper;

	}

	public int CurrentWeaponSlot = 0; 


	public WeaponController.WeaponState weaponAtQ {get; set;}
	public WeaponController.WeaponState weaponAtW {get; set;}
	public WeaponController.WeaponState weaponAtE {get; set;}


	//build utility controller

	public int CurrentUtilSlot = 0;
	public int utilAtX {get; set;}
	public int utilAtC {get; set;}
}

