using System;
using UnityEngine;

//a system for assigning and keeping track of powerups on a ship.
public class PowerUpSystemController
{
	
	PowerUpSystemView _view;
	PowerUpSystemModel _model;


	public PowerUpSystemEvents Events = new PowerUpSystemEvents();
	
	public PowerUpSystemController (PowerUpSystemModel model)
	{
		_model = model;
	}
	
	
	public void Build(GameObject parent)
	{
		_view = parent.AddComponent<PowerUpSystemView>();
		_view.BuildView(_model);

		_view.Events.WeaponSelect += HandleWeaponSelect;
		_view.Events.UtilitySelect += HandleUtilitySelect;
	}

	void HandleUtilitySelect (object sender, EventArgs<int> e)
	{

	}

	void HandleWeaponSelect (object sender, EventArgs<WeaponController.WeaponState> e)
	{
		Events.OnWeaponSelect(sender, e.Value);
	}
	

	public int AssignWeapon(WeaponController.WeaponState ws)
	{
		if(_model.CurrentWeaponSlot == 1)
		{
			_model.weaponAtQ = ws;
		}
		else if(_model.CurrentWeaponSlot == 2)
		{
			_model.weaponAtW = ws;
		}
		else if(_model.CurrentWeaponSlot == 3)
		{
			_model.weaponAtE = ws;
		}
		return _model.CurrentWeaponSlot;
	}

	public void RemovePowerUpAt(int index)
	{
		if(index == 0)
		{
			_model.weaponAtQ = WeaponController.WeaponState.Lasers;
		}
		else if(index == 1)
		{
			_model.weaponAtW = WeaponController.WeaponState.Lasers;
		}
		else if(index == 2)
		{
			_model.weaponAtE = WeaponController.WeaponState.Lasers;
		}
		else
		{
			Debug.LogError("Index out of bounds when removing powerup!!");
		}

		if(index + 1 == _model.CurrentWeaponSlot)
		{
			Events.OnWeaponSelect(this, WeaponController.WeaponState.Lasers);
		}
	}

	//we need this array for the hoarder to search through a take a power up
	public WeaponController.WeaponState[] GetPowerUpArray()
	{
		return new WeaponController.WeaponState[]{_model.weaponAtQ, _model.weaponAtW, _model.weaponAtE};
	}

	public int GetSelectedSlot()
	{
		return _model.CurrentWeaponSlot;
	}
	
}


