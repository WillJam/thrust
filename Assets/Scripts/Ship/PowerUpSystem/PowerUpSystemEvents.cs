using System;

public class PowerUpSystemEvents
{
	public event EventHandler<EventArgs<int>> PowerUpSelect;
	public void OnPowerUpSelect(object sender, int e)
	{
		var handler = PowerUpSelect;
		if(handler != null) handler(this, new EventArgs<int>(e));
	}
	
	public event EventHandler<EventArgs<WeaponController.WeaponState>> WeaponSelect;
	public void OnWeaponSelect(object sender, WeaponController.WeaponState e)
	{
		var handler = WeaponSelect;
		if(handler != null) handler(this, new EventArgs<WeaponController.WeaponState>(e));
	}


	//later this will pass in a UtilState as opposed to an int, build a UtilController much like the WeaponController
	public event EventHandler<EventArgs<int>> UtilitySelect;
	public void OnUtilitySelect(object sender, int e)
	{
		var handler = UtilitySelect;
		if(handler != null) handler(this, new EventArgs<int>(e));
	}
	
}

