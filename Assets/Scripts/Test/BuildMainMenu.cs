using System;
using UnityEngine;

public class BuildMainMenu :MonoBehaviour
{
	void Start()
	{
		
		var mm = new MainMenuModel();
		var mc = new MainMenuController(mm);
		mc.Build();
		
		var bm = new BackgroundModel();
		var bc = new BackgroundController(bm);
		bc.Build();
		bc.BuildDefault();
		bc.UpdateScrollSpeed(0.0f, 0.0f);
	}
}


