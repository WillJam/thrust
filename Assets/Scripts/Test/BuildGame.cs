using System;
using UnityEngine;

public class BuildGame :MonoBehaviour
{
	GameManagerController gm;
	void Start()
	{
		Time.timeScale = 1.0f;
		gm = new GameManagerController();
		gm.LoadGame();
		gm.BuildStartMenu();
	}
	
	void Update()
	{
		gm.Update();
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			Application.Quit();
		}
	}
	void OnGUI()
	{
		GUI.Label(new Rect(0,0,0,0), "");
	}
}


