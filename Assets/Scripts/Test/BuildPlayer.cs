using UnityEngine;
using System.Collections;

public class BuildPlayer : MonoBehaviour {
	
	Vector3 startPosition = new Vector3(0, -10, 0);

	// Use this for initialization
	GibPool _pool = new GibPool();
	void Awake()
	{

	}
	void Start () 
	{
		var shipModel = new ShipModel(true);
		var shipControl = new ShipController("Herbie", shipModel);
		shipControl.Build(startPosition, _pool);

	}
}
