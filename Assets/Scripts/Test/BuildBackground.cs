using UnityEngine;
public class BuildBackground : MonoBehaviour
{
	void Start()
	{
		var bm = new BackgroundModel();
		BackgroundController bC = new BackgroundController(bm);
		bC.Build();
		bC.BuildDefault();
		bC.BuildCelestialBody();
		bC.UpdateScrollSpeed(0.5f, 1);
	}
}


