using UnityEngine;
using System.Collections;

public class DestroyOnInvisible : MonoBehaviour {
	
	void OnBecameInvisible()
	{
		Debug.Log ("laser invisible");
		Destroy(gameObject);
	}
} 
