using System;
using UnityEngine;

public class CameraController
{
	public GameObject CameraObject;
	Camera cam;
	string _title; 
	public CameraController (string name)
	{
		_title = name;
	}
	
	public void Build(Vector3 position, int cull, CameraClearFlags clearFlags, float depth, bool addGuiLayer)
	{
		CameraObject = new GameObject(_title + "_Camera");
		CameraObject.transform.position = position;
		cam = CameraObject.AddComponent<Camera>();
		cam.enabled = false;
		ClearFlags(clearFlags);
		CullingMask(cull);
		Depth(depth);
		if(addGuiLayer)
		{
			CameraObject.AddComponent<GUILayer>();
		}
	}
	
	public void CullingMask(int i)
	{
		cam.cullingMask = 1 << i; 
	}
	
	public void Toggle()
	{
		cam.enabled = !cam.enabled;
	}

	public void Toggle(bool t)
	{
		cam.enabled = t;
	}
	
	public void ClearFlags(CameraClearFlags i)
	{
		cam.clearFlags = i;
	}
	
	public void Depth(float i)
	{
		cam.depth = i;
	}

	public Camera GetCamera()
	{
		return cam;
	}
}


