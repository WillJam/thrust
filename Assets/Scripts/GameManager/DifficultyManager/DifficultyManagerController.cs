using System;
using UnityEngine;
public class DifficultyManagerController
{
	// I need to learn to read xml docs. . . fuck this. 

	EnemyManagerController enemyManager;

	DifficultyManagerModel _model;
	bool _tutorial;

	public DifficultyManagerController (EnemyManagerController enemyManager, bool tutorial)
	{
		this.enemyManager = enemyManager;
		_model = new DifficultyManagerModel();
		_tutorial = tutorial;
		_model.tutorialDiff = 0;
		_model.tutorialWaveNum = -1;

	}

	public void Update()
	{

	}

	public void GetNextWave() 
	{
		float randDif;
		int randWave;
		if(enemyManager == null)
		{
			return;
		}
		if(!_tutorial)
		{
			_model.tutorialDiff = 5;
			randWave = (int)UnityEngine.Random.Range(1, 5);
			randDif = UnityEngine.Random.Range(0.0f, 100.0f);
			Debug.Log ("Difficulty " + randDif);
		}
		else
		{
			_model.tutorialWaveNum += 1;
			if(_model.tutorialWaveNum > 5)
			{
				_model.tutorialDiff +=1;

				_model.tutorialWaveNum = 0;

			}
			randWave = _model.tutorialWaveNum;
			randDif = 150;
		}

		if(_model.tutorialDiff == 0 || randDif < _model.easyRange)
		{
			//EASY
			Debug.Log("easy");
			Debug.Log(randWave);
			switch (randWave)
			{
	
				case 0:
					//0

					enemyManager.BuildWave("Basic", _model.basicPos, 5, 2, 5, 5, 0, 1);
					enemyManager.SetPhaseTime(_model.firstWave);
					break;
				case 1:
					//1
					BasicBlock(1, 0);
					enemyManager.SetPhaseTime(_model.easyWaveLength);
					break;
				case 2:
					//2
					BasicBlock(1, 4);
					BlockerBlock(1, 0);
					enemyManager.SetPhaseTime(_model.bossWave);
					break;
				case 3:
					//3
					StalkerBlock(1, 0);
					enemyManager.SetPhaseTime(_model.easyWaveLength);
					break;
				case 4:
					//4
					BasicBlock(1, 0);
					enemyManager.SetPhaseTime(_model.shortWave);
					break;
				case 5:
					//5
					BasicBlock(1, 0);
					BlockerBlock(1,0);
					enemyManager.SetPhaseTime(_model.easyWaveLength);
					break;
				default:
					Console.WriteLine("Default case");
					break;
			}
			float newEasyRange = _model.easyRange;
			newEasyRange -= 10;
			if(newEasyRange < 10)
			{
				newEasyRange = 10;
			}
			_model.easyRange = newEasyRange;
		}
		else if(_model.tutorialDiff == 1 || (randDif >= _model.easyRange && randDif < _model.medRange) )
		{
			//MEDIUM 
			Debug.Log("med");
			Debug.Log(randWave);
			switch (randWave)
			{

			case 0:
				//0
				SwarmerBlock(1,0);
				enemyManager.SetPhaseTime(_model.bossWave);
				break;
			case 1:
				//1
				Debug.Log("Wave 1 MED");
				BasicBlock(2,0);
				BlockerBlock(1,0);
				StalkerBlock(1,0);
				enemyManager.SetPhaseTime(_model.midWaveLength);
				break;
			case 2:
				//2
				Debug.Log("Wave 2 MED");
				HunterBlock(1,0);
				HunterBlock(1,2);
				enemyManager.SetPhaseTime(_model.bossWave);
				break;
			case 3:
				//3
				HunterBlock(1, 4);
				HunterBlock(1, 5);
				HunterBlock(1, 6);
				HunterBlock(1, 7);
				BlockerBlock(1,0);
				BlockerBlock(1,2);
				enemyManager.SetPhaseTime(_model.midWaveLength);
				break;
			case 4:
				//4
				BlockerBlock(1,0);
				HunterBlock(1, 0);
				HunterBlock(1, 2);
				BasicBlock(2,0);
				StalkerBlock(1,0);
				enemyManager.SetPhaseTime(_model.midWaveLength);
				break;
			case 5:
				//5
				Debug.Log("----------5----------");
				SwarmerBlock(2,0);
				BlockerBlock(1,0);
				HunterBlock(1,2);
				HunterBlock(1,0);
				StalkerBlock(1,5);
				enemyManager.SetPhaseTime(_model.bossWave);
				break;
			default:
				Console.WriteLine("Default case");
				break;
			}
			float newMidRange = _model.medRange;
			newMidRange -= 15;
			if(newMidRange < 30)
			{
				newMidRange = 30;
			}
			_model.medRange = newMidRange;
		}
		else
		{
			//HARD
			Debug.Log("hard");
			Debug.Log(randWave);
			switch (randWave)
			{

			case 0:
				//0
				HoarderBlock(1, 0);
				enemyManager.SetPhaseTime(_model.bossWave);
				break;
			case 1:
				//1
				BasicBlock(2,0);
				HunterBlock(1,1);
				HunterBlock(1,3);
				HunterBlock(1,5);
				HunterBlock(1,7);
				BlockerBlock(2,0);
				StalkerBlock(2,0);
				enemyManager.SetPhaseTime(_model.hardWaveLength);
				break;
			case 2:
				//2
				BlockerBlock(1,0);
				BlockerBlock(1,0);
				HoarderBlock(1, 0);
				enemyManager.SetPhaseTime(_model.hardWaveLength);
				break;
			case 3:
				//3
				BasicBlock(2, 0);
				BlockerBlock(1,0);
				HunterBlock(1,0);
				HunterBlock(1,2);
				StalkerBlock(1,0);
				enemyManager.SetPhaseTime(_model.hardWaveLength);
				break;
			case 4:
				//4
				SwarmerBlock(2,0);
				BlockerBlock(1,0);
				HunterBlock(1,0);
				HunterBlock(1,2);
				enemyManager.SetPhaseTime(_model.hardWaveLength);
				break;
			case 5:
				//5
				SwarmerBlock(2,0);
				BlockerBlock(1,0);
				HunterBlock(1,0);
				HunterBlock(1,2);
				HoarderBlock(1, 0);
				enemyManager.SetPhaseTime(_model.hardWaveLength);
				_tutorial = false;
				_model.tutorialDiff = 4;
				break;
			default:
				Console.WriteLine("Default case");
				break;
			}
		}
	}
	
	void BasicBlock(int diff, int t)
	{
		enemyManager.BuildWave("Basic", _model.basicPos, 5, 5, 5, 5, diff,t);
	}

	void StalkerBlock(int diff, int t)
	{
		enemyManager.BuildWave("Stalker", _model.stalkerPos, 1, 1, 1, 1, diff,t);
	}

	void BlockerBlock(int diff, int t)
	{
		enemyManager.BuildWave("Blocker", _model.blockerPos, 1, 1, 1, 1, diff,t);
	}

	void SwarmerBlock(int diff, int t)
	{
		enemyManager.BuildWave("Swarmer", _model.swarmerPos, 5,5,5,5,diff,t);
	}

	void HunterBlock(int diff, int t)
	{
		enemyManager.BuildWave("Hunter", _model.hunterPos, 1, 1, 1, 1, diff,t);
	}

	void HoarderBlock(int diff, int t)
	{
		enemyManager.BuildWave("Hoarder", _model.hoarderPos, 1, 1, 1, 1, diff,t);
	}
}


