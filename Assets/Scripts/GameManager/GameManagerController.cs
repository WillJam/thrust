using System;
using System.Collections.Generic;
using UnityEngine;
public class GameManagerController
{
	GameManagerModel _model;
	
	//write this into a saved file that we can read at the games start-up
	bool tutorial = true; 
	bool shipReady = false;
	bool gameOver = false;
	
	MainMenuController menuControl;
	BackgroundController backgroundControl;
	ShipController shipController;
	ShipModel sM;
	EnemyManagerController enemyManager;
	EnemyManagerModel emM;
	DifficultyManagerController diffManager;

	GameOverController gameOverController; 

	GibPool _pool;

	SoundController sounds;

	SoundTrackController soundtrack;

	int waveNum = 0;
	
	public GameManagerController ()
	{
		_model = new GameManagerModel();
		_pool = new GibPool();

		soundtrack = new SoundTrackController();
	}
	
	public void LoadGame()
	{
		//BUILD ALL MODELS RIGHT HERE 


		//READ bool tutorial 

		sM = new ShipModel(tutorial);

		shipController = new ShipController("Herbs", sM);
		shipController.Build(_model.shipStartPos, _pool);
		shipController.SetControls(false);
		shipReady = false;
		waveNum = 0;

		emM = new EnemyManagerModel(shipController);
		enemyManager = new EnemyManagerController("Master Mind", emM, _pool);

		diffManager = new DifficultyManagerController(enemyManager,tutorial);

		sounds = new SoundController();
		sounds.Build(new GameObject("sounds"), false, false);

		soundtrack.Build(tutorial);


		var gOM = new GameOverModel();
		gameOverController = new GameOverController(gOM);
		gameOverController.Build();


		enemyManager.Events.UpdateScore += HandleUpdateScore;
		shipController.Events.Death += HandleHerbieDeath;
		_model.score = 0;
		shipController.UpdateScore(_model.score);
		
	}

	void HandleUpdateScore (object sender, EventArgs<int> e)
	{
		_model.score += e.Value; 
		//Debug.Log(_model.score);
		shipController.UpdateScore(_model.score);
	}
	
	public void BuildStartMenu()
	{
		var mm = new MainMenuModel();
		menuControl = new MainMenuController(mm);
		menuControl.Build();
		
		var bm = new BackgroundModel();
		backgroundControl = new BackgroundController(bm);
		backgroundControl.Build();
		backgroundControl.BuildDefault();
		backgroundControl.UpdateScrollSpeed(_model.menuScrollSpeed, 1);
		
		menuControl.Events.SpacePressed += HandleStart;
		menuControl.Events.ControlSpacePressed += HanldeTutorialStart;

		soundtrack.MenuStart();

	}
	
	
	public void Update()
	{
		if(shipReady)
		{
			enemyManager.Update();
			diffManager.Update();
		}

	}

	void HanldeTutorialStart (object sender, EventArgs e)
	{
		tutorial = true;
		Debug.Log("tut start");
		HandleStart(sender, e);
	}

	void HandleStart (object sender, EventArgs e)
	{		
;
		//leave first playthrough as false for now!!
		//once intro is finished send an event

		IntroController iC = new IntroController(shipController, backgroundControl);
		iC.Build();
		soundtrack.GameStart ();
		iC.Events.ShipReady += HandleShipReady;
	}

	void HandleHerbieDeath (object sender, EventArgs e)
	{
		Debug.Log ("Herbie died.");
		sounds.PlayShot(_model.herbiedeath, 0.2f);
		gameOver = true;
		gameOverController.Events.Restart += HandleRestart;
		gameOverController.GameOver(_model.score);

		Time.timeScale = 0.1f; 
	}
	

	void HandleRestart (object sender, EventArgs<GameOverController> e)
	{
		CleanUp();
		enemyManager.KillAll();
		enemyManager.Destroy();
		gameOverController.Destroy();

		tutorial = false;
		LoadGame();

		Time.timeScale = 1;
		HandleStart(this, EventArgs.Empty);
	}

	void CleanUp()
	{
		//used for restarting

		foreach(var x in GameObject.FindGameObjectsWithTag("baselaser"))
		{
			GameObject.Destroy(x);
		}
		foreach(var x in GameObject.FindGameObjectsWithTag("hunterbullet"))
		{
			GameObject.Destroy(x);
		}
		foreach(var x in GameObject.FindGameObjectsWithTag("enemybaselaser"))
		{
			GameObject.Destroy(x);
		}
		foreach(var x in GameObject.FindGameObjectsWithTag("machinebullet"))
		{
			GameObject.Destroy(x);
		}
		foreach(var x in GameObject.FindGameObjectsWithTag("SeekerShot"))
		{
			GameObject.Destroy(x);
		}
		foreach(var x in GameObject.FindGameObjectsWithTag("enemybullet"))
		{
			GameObject.Destroy(x);
		}
		foreach(var x in GameObject.FindGameObjectsWithTag("enemybullet"))
		{
			GameObject.Destroy(x);
		}
		foreach(var x in GameObject.FindGameObjectsWithTag("mini"))
		{
			GameObject.Destroy(x);
		}
	}
	
	
	//this is the event handler that is sent when the intro sequence is complete
	void HandleShipReady (object sender, EventArgs e)
	{
		//HERE IS WHERE WE START THE ACTUAL GAME, ALL LEVELS MUST BE BUILT AT THIS POINT AND READY TO BE PLAYED 
		//ENEMY SPAWN TIMES AND FORMATIONS SHOULD BE BUILT ***!*!*!*!)(@*)(!*@)(!@*
		
		if(!shipReady)
		{		

			enemyManager.Build();
			enemyManager.Events.WaveDefeated += HandleWaveDefeated;
			enemyManager.Events.WaveTimerExpired += HandleWaveTimeExpired;

			shipController.SetControls(true);
			shipController.InitShipEvents();
			shipController.ShowHUD(0.2f);
			shipReady = true;
			ChooseWave();

		}
	}

	void HandleWaveTimeExpired (object sender, EventArgs e)
	{
		//some temp level design
		Debug.Log ("Wave timer expired -- - - - New Wave");
		//some temp stuff to test with 
		ChooseWave();
	}

	void HandleWaveDefeated (object sender, EventArgs e)
	{
		Debug.Log("Wave Defeated - - - -  New Wave");
		//some temp stuff to test with 
		ChooseWave();

	}
	void ChooseWave()
	{
		diffManager.GetNextWave();
	}
}

