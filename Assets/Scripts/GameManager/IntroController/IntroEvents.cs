using System;


public class IntroEvents
{
	public event EventHandler<EventArgs> ShipReady;
	public void OnShipReady()
	{
		var handler = ShipReady;
		if(handler != null) handler(this, EventArgs.Empty);
	}
}


