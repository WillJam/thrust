using System;
using UnityEngine;

public class IntroModel
{

	public Vector3 shipEndPos = new Vector3(0, -14, 0);
	public float introScrollSpeed = 1f;
	public float introScrollAcceleration = 0.5f;
	
	public float vertSpeed = 10.0f;
	
	//use these for the alternate tutorial intro
	public float tutorialIntroScrollSpeed = 1.0f;
	public float tutorialIntroScrollAcceleration = 0.3f;
	
	public float heightPadding = 170; 
	
	public IntroModel()
	{


	}
}


