using System;
using UnityEngine;
public class IntroController
{
	ShipController _ship; 
	BackgroundController _background;
	
	IntroModel _model;
	IntroView _view; 
	public IntroEvents Events = new IntroEvents();
	
	bool _tutorial;
	public IntroController (ShipController ship, BackgroundController background)
	{
		_ship = ship;
		_background = background;
		_model = new IntroModel();
	}
	
	public void Build()
	{
		
		//build an intro view with tutorial view variables
		_view = _ship.ShipObject.AddComponent<IntroView>();
		_view.BuildView(_model.vertSpeed);
		
		//build different tutorial backgrounds
		_background.UpdateScrollSpeed(_model.introScrollSpeed, _model.introScrollAcceleration);
		_background.Events.BackgroundUpdated += HandleBackgroundUpdated;
		
		
		_view.Events.ShipReady += HandleShipReady;

	}

	void HandleShipReady (object sender, EventArgs e)
	{
		Events.OnShipReady();
	}

	void HandleBackgroundUpdated (object sender, EventArgs e)
	{
		_view.MoveDown = true;
	}
}

