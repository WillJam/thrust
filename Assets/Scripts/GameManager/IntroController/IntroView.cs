using System;
using UnityEngine;
public class IntroView :MonoBehaviour
{
	
	public bool MoveDown = false;
	private float _verticalSpeed;
	
	public IntroEvents Events = new IntroEvents();
	public IntroView ()
	{
		
	}
	
	public void BuildView(float verticalSpeed)
	{
		_verticalSpeed = verticalSpeed;
	}
	
	
	void Update()
	{
		if(MoveDown)
		{
			var newPos = transform.position;
			newPos.y += _verticalSpeed * Time.deltaTime;
			if(newPos.y < -14){
				transform.position = newPos;
				_verticalSpeed -= 2f * Time.deltaTime;
			} 
			else 
			{
				MoveDown = false;
				_verticalSpeed = 0; 
				//if tutorial then do tutorial otherwise they are ready to play 
				
					// put tut stuff here and eventually do the else statement
				//else 
				Events.OnShipReady();
				Destroy(this);
			}
		}
	}
}


