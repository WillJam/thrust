using UnityEngine;
public class SoundController
{
	public AudioSource src {get; set;}
	public bool timeExpired = false;


	public SoundEvents Events = new SoundEvents();
	public SoundController ()
	{
	}
	
	public void Build(GameObject parent, bool onAwake, bool loop)
	{
		src = parent.AddComponent<AudioSource>();
		src.playOnAwake = onAwake;
		src.loop = loop;
	}

	public void PlayShot(AudioClip clip)
	{
		src.PlayOneShot(clip);
	}
	
	public void PlayShot(AudioClip clip, float volume)
	{
		src.PlayOneShot(clip, volume);
	}

	public void SetVolume(float volume)
	{
		src.volume = volume;
	}

	public void SetPitch(float pitch)
	{
		src.pitch = pitch;
	}

	public void SetClip(AudioClip clip)
	{
		src.clip = clip;
	}

	public void SetLoop(bool loop)
	{
		src.loop = loop;
	}

	public bool GetIsPlaying()
	{
		return src.isPlaying;
	}

	public void PlayLoop(AudioClip clip, double time)
	{
		PlayLoop(clip,time,0);
	}

	public void PlayLoop(AudioClip clip, double time, float delay)
	{
		src.clip = clip;
		if(!src.isPlaying)
		{
			src.PlayDelayed(delay);
			//src.SetScheduledEndTime(time);
		}
		else
		{
			if(src.time > time)
			{
				//Time expired. Send event or bool flag.
				timeExpired = true;
			}
			else
			{
				//Time not expired. 
				timeExpired = false;
			}
		}
	}

	public void Stop()
	{
		src.time = 0;
		src.Stop();
	}
	
	public void FadeAudio(float time, float volume)
	{
		iTween.AudioTo(src.gameObject, volume, src.pitch, time);
	}

	public void FadeAudio(float time, float volume, float pitch)
	{
		iTween.AudioTo(src.gameObject, volume, pitch, time);
	}

	public void FadeAudioFrom(float time, float volume)
	{
		iTween.AudioFrom(src.gameObject, volume, src.pitch, time);
	}


	public void RandomizePitch(float min, float max)
	{
		src.pitch = Random.Range(min, max);
	}
}


