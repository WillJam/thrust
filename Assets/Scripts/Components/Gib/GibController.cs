using UnityEngine;


public class GibController
{
	GameObject gameObject;
	Rigidbody rigidBody;
	
	float lifeTime; 
	
	GibView _view;

	bool firstUse = true;
	public bool inUse; 

	GibPool _pool;
	
	private Texture3DController textureControl;
	public GibController (GibPool pool)
	{
		_pool = pool;
	}
	
	public void Build(Vector3 position, float radius, float explosionForce)
	{
		lifeTime = Random.Range(1.0f, 2.0f);
		explosionForce = Random.Range(explosionForce, explosionForce + 100.0f);
		gameObject = new GameObject("Gib");
		gameObject.transform.position = position + Random.insideUnitSphere * radius;
		
		_view = gameObject.AddComponent<GibView>();
		_view.lifeTime = lifeTime;

		_view.Events.Destroy += HandleDestroy;
		
		textureControl = new Texture3DController("Gib/GibTexture", "Gib");
		textureControl.Build(gameObject);
		
		textureControl.Events.BecameInvisible += HandleBecomeInvisible;
			
		rigidBody = gameObject.AddComponent<Rigidbody>();
		rigidBody.drag = 0.0f;
		rigidBody.useGravity = false;
		rigidBody.detectCollisions = true;
		rigidBody.AddExplosionForce(explosionForce, position, radius);
		
	}

	public void Reuse(Vector3 position, float radius, float explosionForce)
	{
		inUse = true;
		gameObject.transform.position = position + Random.insideUnitSphere * radius;
		explosionForce = Random.Range(explosionForce, explosionForce + 100.0f);
		rigidBody.AddExplosionForce(explosionForce, position, radius);
		_view.lifeTime = lifeTime;
	}

	void HandleDestroy (object sender, System.EventArgs e)
	{
		inUse = false;
		if(firstUse)
		{
			_pool.Add(this);
			firstUse = false;
		}
	}

	void HandleBecomeInvisible (object sender, System.EventArgs e)
	{
		_view.Destroy();
	}
}


