using System;
using UnityEngine;

public class GibView : MonoBehaviour
{
	public float lifeTime;
	public GibEvents Events = new GibEvents();
	void Update()
	{
		if(lifeTime > 0)
		{
			lifeTime -= Time.deltaTime;
		} 
		else
		{

			Destroy();
		}
	}
	
	public void Destroy()
	{
		//remove any velocity
		Events.OnDestroy(this);
		rigidbody.velocity = Vector3.zero;
		transform.position = new Vector3(5000,5000,5000);
	}
}

