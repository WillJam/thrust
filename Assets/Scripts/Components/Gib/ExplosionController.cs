using System;
using UnityEngine;


public class ExplosionController
{
	public ExplosionController ()
	{
		
	}
	
	public void Build(Vector3 position, int numGibs, float radius, float explosionForce, GibPool pool)
	{
		for(int i = 0; i < numGibs; i++)
		{
			GibController g = pool.FindUnusedGib();
			if(g == null)
			{
				g = new GibController(pool);
				g.Build(position, radius, explosionForce);
			}
			else
			{
				g.Reuse(position, radius, explosionForce);
			}

		}
	}
}


