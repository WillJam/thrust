using UnityEngine;
public class TextureController
{
	public GameObject textureObject;
	public Vector2 Size {get; set;}
	GUITexture mainTexture; 
	string title;
	
	public TextureController (string name)
	{
		title = name;
	}
	
	public void Build(GameObject parent, Vector3 pos, Vector3 size, Texture t)
	{
		textureObject = new GameObject(title);
		Size = size;
		
		textureObject.transform.parent = parent.transform;
		textureObject.transform.localPosition = pos;
		textureObject.transform.localScale = Size;
		mainTexture = textureObject.AddComponent<GUITexture>();
		mainTexture.texture = t;
	}

	public void Position(Vector3 pos)
	{
		textureObject.transform.localPosition = pos;
	}
	
	public void ChangeTexture(Texture t){
		
		mainTexture.texture = t;
	}
	
	public void Show(float t)
	{
		Show (t, 1);
	}

	public void Show(float t, float a)
	{
		iTween.FadeTo(textureObject, a, t);
	}
	
	public void Hide(float t)
	{
		iTween.FadeTo(textureObject, 0, t);
	}
}


