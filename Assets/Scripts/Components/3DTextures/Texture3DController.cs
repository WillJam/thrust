 using System;
using UnityEngine;
public class Texture3DController
{
	
	
	private Texture3DModel _model; 
	private Texture3DView _view; 
	public GameObject modelObject; 
	public Collider collider {get; set;}
	
	private string _fullPathName;
	private string _objectName;
	
	public Texture3DEvents Events = new Texture3DEvents();

	public Material mat;
	
	//might want to think about storing a renderer/mesh collider here
	//(1) so we can esily get info for collisions! annd (2) so we can
	//change the mesh collider in case a textures under goes an animation pr changes altogether
	
	public Texture3DController (string pathName, string objectName)
	{
		_model = new Texture3DModel();
		_objectName = objectName + "_Texture";
		_fullPathName = _model.defaultTexturePath + pathName; 
	}
	
	public void Build(GameObject parent)
	{
		modelObject = (GameObject) GameObject.Instantiate(Resources.Load(_fullPathName));
		if(modelObject == null)
		{
			Debug.Log (_fullPathName);
		}

		modelObject.name = _objectName;
		modelObject.transform.position = Vector3.zero;
		if(parent != null)
		{
			modelObject.transform.parent = parent.transform;
			modelObject.transform.localPosition = Vector3.zero;
		}
		collider = modelObject.GetComponentInChildren<Collider>();

		if(modelObject.renderer != null)
		{
			mat = modelObject.renderer.material;
		}


		if(collider == null)
		{
			collider = modelObject.collider;
		}
		
		//build view
		if(collider != null){
			_view = collider.gameObject.AddComponent<Texture3DView>();

		}
		else
		{
			_view = modelObject.AddComponent<Texture3DView>();
		}
		//events
		//send the event up to the textures parent if needed
		_view.Events.BecameInvisible += HandleOnBecameInvisible;
		_view.Events.TriggerEnter += HandleTriggerEnter;
		_view.Events.TriggerStay += HandleTriggerStay;
		_view.Events.TriggerExit += HandleTriggerExit;
	}

	void HandleTriggerExit (object sender, EventArgs<GameObject> e)
	{
		//Debug.Log("Exit");
		Events.OnTriggerExit(e.Value);
	}

	void HandleTriggerStay (object sender, EventArgs<GameObject> e)
	{
		Events.OnTriggerStay(e.Value);
	}

	public void Position(Vector3 newPos)
	{
		modelObject.transform.localPosition = newPos;
	}

	public void Scale(Vector3 newScale)
	{
		modelObject.transform.localScale = newScale;
	}

	public void Rotation(Quaternion rot)
	{
		modelObject.transform.localRotation = rot;
	}

	public void Flash()
	{
		_view.FlashTexture();
	}

	public void TweenColor(Color b, float time)
	{
		iTween.ColorTo(modelObject, b, time);
	}

	public void Parent(GameObject newParent)
	{
		modelObject.transform.parent = newParent.transform;
		modelObject.transform.localPosition = Vector3.zero;
	}


	void HandleTriggerEnter (object sender, EventArgs<GameObject> e)
	{
		Events.OnTriggerEnter(e.Value);	
	}
	
	void HandleOnBecameInvisible (object sender, EventArgs e)
	{
		Events.OnBecameInvisible();
	}

	public void SetActive(bool t)
	{
		modelObject.SetActive(t);
	}

	public void DestroyMe()
	{
		_view.DestroyMe();
	}
}

