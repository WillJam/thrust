using System;
using UnityEngine;
using System.Collections;

public class Texture3DView : MonoBehaviour
{
	public Texture3DEvents Events = new Texture3DEvents();

	float triggerStayTime = 1f;

	new Renderer renderer; 
	Color collideColor;
	Color normalColor;

	void Start()
	{
		renderer = GetComponentInChildren<MeshRenderer>();
		collideColor = Color.red;
		normalColor = Color.white;
	}

	void OnBecameInvisible()
	{
		Events.OnBecameInvisible();
	}
	
	void OnTriggerEnter(Collider other)
	{
		Events.OnTriggerEnter(other.gameObject);
	}

	void OnTriggerStay(Collider other)
	{
		if(triggerStayTime > 0)
		{
			triggerStayTime -= Time.deltaTime;
		}
		else
		{
			Events.OnTriggerStay(other.gameObject);
			triggerStayTime = 1f;
		}
	}

	void OnTriggerExit(Collider other)
	{
		Events.OnTriggerExit(other.gameObject);
		triggerStayTime = 1;
	}

	public void FlashTexture()
	{
		if(gameObject.activeSelf)
		{
			StartCoroutine(Flash ());
		}

	}

	IEnumerator Flash()
	{
		renderer.material.color = collideColor;
		yield return new WaitForSeconds(.1f);
		renderer.material.color = normalColor;
		yield return new WaitForSeconds(.1f);
		renderer.material.color = collideColor;
		yield return new WaitForSeconds(.1f);
		renderer.material.color = normalColor;
	}

	public void DestroyMe()
	{
		Destroy(gameObject);
	}
}


