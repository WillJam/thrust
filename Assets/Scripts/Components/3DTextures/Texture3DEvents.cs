using System;
using UnityEngine;
public class Texture3DEvents
{
	public event EventHandler BecameInvisible;
	public void OnBecameInvisible()
	{
		var handler = BecameInvisible;
        if (handler != null) handler(this, EventArgs.Empty);
	}
	
	public event EventHandler<EventArgs<GameObject>> TriggerEnter;
	public void OnTriggerEnter(GameObject g)
	{
		var handler = TriggerEnter;
		if (handler != null) handler(this, new EventArgs<GameObject>(g));
	}

	public event EventHandler<EventArgs<GameObject>> TriggerStay;
	public void OnTriggerStay(GameObject g)
	{
		var handler = TriggerStay;
		if (handler != null) handler(this, new EventArgs<GameObject>(g));
	}

	public event EventHandler<EventArgs<GameObject>> TriggerExit;
	public void OnTriggerExit(GameObject g)
	{
		var handler = TriggerExit;
		if (handler != null) handler(this, new EventArgs<GameObject>(g));
	}
}

