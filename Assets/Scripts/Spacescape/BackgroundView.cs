using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public class BackgroundView : MonoBehaviour
{
	public BackgroundEvents Events = new BackgroundEvents();
	public float scrollSpeed;
	public float offScreen;
	
	
	bool updating = false;
	bool accelerating = false;
	float _newSpeed;
	float _rate;
	
	
	public void Build(float speed, float OffScreen)
	{		
		scrollSpeed = speed;
		offScreen = OffScreen;
	}
	void Update()
	{
		Vector3 newPos = gameObject.transform.position;
		newPos.y -= scrollSpeed * Time.deltaTime;
		if(newPos.y < offScreen){
			Events.OnBackgroundCycle(gameObject);
		}  
		else
		{
			gameObject.transform.position = newPos;
		}
		
		
		if(updating)
		{
			if(accelerating)
			{
				if(scrollSpeed < _newSpeed)
				{
					scrollSpeed += _rate *Time.deltaTime;
				} 
				else
				{
					updating = false;
					Events.OnBackgroundUpdated();
				}
			}
			else 
			{
				if(scrollSpeed > _newSpeed)
				{
					scrollSpeed -= _rate *Time.deltaTime;
				} 
				else
				{
					updating = false;
					Events.OnBackgroundUpdated();
				}
			}

		}
	}
	
	public void UpdateScrollSpeed(float newSpeed, float rate)
	{
		_newSpeed = newSpeed;
		_rate = rate;
		if(scrollSpeed < _newSpeed)
		{
			accelerating = true;
		}
		else
		{
			accelerating = false;
		}
		updating = true;
		
	}
	

}


