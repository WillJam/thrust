using System;
using UnityEngine;
public class CelestialBodyController
{
	
	GameObject gameObject;
	TextureController bodyTexture;
	BackgroundView _view;
	public float offScreen = -10.0f;
		
	public CelestialBodyController ()
	{
		
	}
	
	public void Build(Texture texture, float scrollSpeed, Vector3 position)
	{
		gameObject = new GameObject("CelestialBody");
		_view = gameObject.AddComponent<BackgroundView>();
		bodyTexture = new TextureController("CelestialBodyTexture");
		bodyTexture.Build(gameObject, position, new Vector3(0.5f, 0.8f, 1f), texture);
		bodyTexture.textureObject.layer = 13;
		
		_view.Build(scrollSpeed,offScreen);
		_view.Events.BackgroundCycle += HandleCycle;
		
	}

	void HandleCycle (object sender, EventArgs e)
	{
	}
	
}

