using System;
using UnityEngine;
public class BackgroundEvents
{
	public event EventHandler<EventArgs> BackgroundCycle;
	public void OnBackgroundCycle(GameObject go)
	{
		var handler = BackgroundCycle;
        if (handler != null) handler(this, EventArgs.Empty);
	}
	
	public event EventHandler<EventArgs> BackgroundUpdated;
	public void OnBackgroundUpdated()
	{
		var handler = BackgroundUpdated;
		if(handler != null) handler(this, EventArgs.Empty);
	}
}

