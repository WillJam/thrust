using System;
using System.Collections;
using UnityEngine;
using System.Collections.Generic;

public class BackgroundController
{
	GameObject BackgroundWall;
	CameraController BackgroundCamera;
	
	BackgroundView _view;
	BackgroundModel _model;
	public BackgroundEvents Events;
	//temp!!!!
	bool t;
	
	public BackgroundController (BackgroundModel model)
	{
		//loading stuff maybe have this done in a loader in the beginning, then pass it into background control model 
		_model = model;
	}
	
	public void Build()
	{
		BackgroundCamera = new CameraController("Background");
		BackgroundCamera.Build(new Vector3(0, 1, -25), 13, CameraClearFlags.SolidColor, 0, true);
		BackgroundCamera.Toggle();
		_model.textureList = new List<TextureController>();
		
		BackgroundWall = new GameObject("BackgroundWall");
		_view = BackgroundWall.AddComponent<BackgroundView>();
		_view.Build(_model.scrollSpeed, _model.offScreen);
		
		
		Events = _view.Events;
		_view.Events.BackgroundCycle += HandleBackgroundCycle;
	}

	void HandleBackgroundCycle (object sender, EventArgs e)
	{
		//TEMP STUFF COME UP WITH A BETTER SYSTEM FOR THIS SHIAT
		//Check out the level to see what textures to use, see if the textures have been used before, roll tha dice,
		//use the texture, mark the texture as having been used
		Vector3 newPos = _view.gameObject.transform.position;
		newPos.y = 7.5f;
		_view.gameObject.transform.position = newPos;
		if(t)
		{

			//tc.ChangeTexture(backgroundDictionary["nebula1"]);
			t = !t;
		} 
		else
		{
			//tc.ChangeTexture(backgroundDictionary["star1"]);
			t = !t;
		}
	}
	
	//just normal star backgrounds
	public void BuildDefault()
	{
		Vector3 newPos = new Vector3(0.5f, 3, 0);
		for(int i = 0; i < 10; i++)
		{
			var tc = new TextureController("bgTexture");
			Texture t = _model.backgroundDictionary["star1"];
			tc.Build(BackgroundWall, newPos, new Vector3(1, 2f, 1), t);
			tc.textureObject.layer = 13;
			_model.textureList.Add(tc);
			newPos.y -= 2f;
		}
	}
	
	public void BuildCelestialBody()
	{
		//temp. . . randomize and choose an unused celestial body
		var cB = new CelestialBodyController();
		cB.Build(_model.celestialDictionary["bluesun"], 0.7f, new Vector3(0.2f, 8, 1));
	}
	
	public void UpdateScrollSpeed(float newScroll, float rate)
	{
		_model.scrollSpeed = newScroll;
		_view.UpdateScrollSpeed(_model.scrollSpeed, rate);
	}
}
 

