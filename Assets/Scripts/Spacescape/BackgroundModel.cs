using System;
using System.Collections;
using UnityEngine;
using System.Collections.Generic;

public class BackgroundModel
{
	
	public Dictionary<string, Texture> backgroundDictionary;
	public Dictionary<string, Texture> celestialDictionary;
	public List<TextureController> textureList;
	
	//TEMP STUFF NEEDS GENERALIZIng maybe
	public float scrollSpeed = 0f;
	public float offScreen = -1.5f;
	public BackgroundModel ()
	{
		backgroundDictionary = new Dictionary<string, Texture>();
		backgroundDictionary.Add("star1", (Texture) Resources.Load ("Textures/Spacebg"));
		backgroundDictionary.Add ("nebula1", (Texture) Resources.Load ("Textures/Spacebg_nebula"));
		
		
		celestialDictionary = new Dictionary<string, Texture>();
		celestialDictionary.Add("bluesun", (Texture) Resources.Load ("Textures/blueplanet"));
		celestialDictionary.Add("greensun", (Texture) Resources.Load ("Textures/greenplanet"));
		celestialDictionary.Add("sun", (Texture) Resources.Load ("Textures/Sun"));
	}
}


