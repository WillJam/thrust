using System;
using UnityEngine;


public class ProjectileController
{
	GameObject gameObject;
	ProjectileModel _model; 
	ProjectileView _view;
	
	string projectileName;
	
	private Texture3DController textureControl;

	GibPool _pool;
	
	public ProjectileController (string name, ProjectileModel model, GibPool pool)
	{
		_model = model;
		projectileName = name;
		_pool = pool;
	}
	
	public void Build(Vector3 startPos)
	{
		gameObject = new GameObject(projectileName);
		gameObject.transform.localPosition = startPos;
		
		//build 3D model 
		textureControl = new Texture3DController(_model.PathName, gameObject.name);
		textureControl.Build(gameObject);
		
		//build view
		_view = gameObject.AddComponent<ProjectileView>();
		_view.Build(_model.Speed, _model.Direction, _model.Rotation);
		
		//events 
		textureControl.Events.BecameInvisible += HandleBecameInvisible;
		textureControl.Events.TriggerEnter += HandleTriggerEnter;
		
	}

	public void ShootAt(Transform source, Transform destination, float speed)
	{
		var rigidbody = textureControl.modelObject.GetComponent<Rigidbody>();
		rigidbody.velocity = (destination.position - source.position).normalized  * speed;
	}

	void HandleBecameInvisible (object sender, EventArgs e)
	{
		_view.Destroy();
	}
	
	void HandleTriggerEnter (object sender, EventArgs<GameObject> e)
	{
		//Temp stuff to see what it's like to destroy an object 
		//GameObject.Destroy(e.Value);
		var ExplosionController = new ExplosionController();
		ExplosionController.Build(gameObject.transform.position, _model.numGibs, 0.1f, _model.explosionForce, _pool);
		_view.Destroy();
		//GOAL : 
		//add gibs/particle effects on explosions!!!!!!@!!!^%$#*&^5349824pqehlfkajh
	}
}


