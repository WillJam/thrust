using UnityEngine;
public class ProjectileView : MonoBehaviour
{
	
	private Vector3 _projectileSpeed = Vector3.zero;
	private Vector3 _rotation = Vector3.zero;
	private float _direction = 0;
	
	public ProjectileView ()
	{
		
	}
	
	public void Build(Vector3 projectileSpeed, float dir, Vector3 rotation)
	{
		_projectileSpeed = projectileSpeed;
		_rotation = rotation;
		gameObject.transform.rotation = Quaternion.Euler(rotation);
		_direction = dir; 
	}

	
	void Update()
	{
		//temporary code to get the bullets actually moving
		//need to replace with variable methods that allow the direction and rotation of the bullets to 
		//be adjusted 
		if(_projectileSpeed != Vector3.zero)
		{
			var newPos = gameObject.transform.position;
			newPos += Time.deltaTime * _projectileSpeed * _direction;
			gameObject.transform.position = newPos;
		}
		//end temp code
	}
	
	
	public void Destroy()
	{
		Destroy(gameObject);
	}
}


