using UnityEngine;
public class ProjectileModel
{
	//needs setters and getters for projectile speed variability 
	private readonly string defaultProjectilePath = "Projectiles/";
	
	private Vector3 _speed;
	public Vector3 Speed{get { return _speed; } set { _speed = value;}}
	
	private float _dir;
	public float Direction { get { return _dir;} set {_dir = value;}}
	
	private string _pathName; 
	public string PathName { get { return _pathName;} set {_pathName = value;}} 
	
	public int numGibs {get; set;}
	public float explosionForce {get; set;}
	
	public Vector3 Rotation {get; set;}
	
	public ProjectileModel(Vector3 Speed, float Dir, string PathName, Vector3 rotation)
	{
		_speed = Speed;
		_dir = Dir;
		_pathName = defaultProjectilePath + PathName;
		Rotation = rotation;
		numGibs = 5;
		explosionForce = 250.0f;
	}
	
	public ProjectileModel(Vector3 Speed, float Dir, string PathName,  Vector3 rotation, int NumGibs, float ExplosionForce)
	{
		_speed = Speed;
		_dir = Dir;
		_pathName = defaultProjectilePath + PathName;
		Rotation = rotation;
		numGibs = NumGibs;
		explosionForce = ExplosionForce;
	}
}


