// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using System;
using UnityEngine;
public class PowerUpModel
{

	public bool BeingHoarded;
	public WeaponController.WeaponState PowerUpKey;
	public Transform TweenTarget; 

	public PowerUpModel (ShipController ship) : this(false, null, DetermineKey(), ship)
	{

	}

	public PowerUpModel (bool beingHoarded, Transform tweenTarget, WeaponController.WeaponState powerUpKey, ShipController ship)
	{
		BeingHoarded = beingHoarded;
		TweenTarget = tweenTarget;
		if(ship.CheckPowerUpArray(powerUpKey))
		{
			PowerUpKey = WeaponController.WeaponState.Healthpak;
		}
		else
		{
			PowerUpKey = powerUpKey;
		}


	}

	static WeaponController.WeaponState DetermineKey()
	{
		int chance = UnityEngine.Random.Range(0, 100);
		//Debug.Log("Chance" + chance);
		if(chance >= 0 && chance < 25)
		{
			return WeaponController.WeaponState.TriBeam;
		}
		else if(chance >= 25 && chance < 50)
		{
			return WeaponController.WeaponState.Beam;
		}
		else if(chance >= 50 && chance < 70)
		{
			return WeaponController.WeaponState.Clipper;
		}
		else if(chance >= 70 && chance < 90)
		{
			return WeaponController.WeaponState.MachineGun;
		}
		else if(chance >= 90 && chance <= 100)
		{
			return WeaponController.WeaponState.SeekerShot;
		}
		else
		{
			Debug.LogError ("Something went wrong with power up selection!!");
			return WeaponController.WeaponState.Lasers;
		}
	}

	public float tweenTime = 20.0f;

}


