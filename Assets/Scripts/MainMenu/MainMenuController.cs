using System;
using UnityEngine;
public class MainMenuController
{
	GameObject mainMenuPanel; 
	TextureController thrustTexture;
	TextureController startTexture;
	
	MainMenuModel _model;
	MainMenuView _view;
	
	bool hide;
	
	public MainMenuEvents Events;
	
	public MainMenuController (MainMenuModel model)
	{
		_model = model;
	}
	
	public void Build()
	{
		mainMenuPanel = new GameObject("Main_menu");
		_view = mainMenuPanel.AddComponent<MainMenuView>();
		
		thrustTexture = new TextureController("Title");
		thrustTexture.Build(mainMenuPanel, _model.titlePos, _model.titleSize, _model.titleTexture);
		
		startTexture = new TextureController("Start");
		startTexture.Build(mainMenuPanel, _model.startPos, _model.startSize, _model.pressStartTexture);
		
		_view.Events.ShowHideCycle += HandleShowHide;
		_view.Events.SpacePressed += HandleSpacePressed;
		_view.Events.ControlSpacePressed += HandleSpacePressed;
		Events = _view.Events;
	}
	
	void HandleSpacePressed (object sender, EventArgs e)
	{
		_view.gameStart = true;
		startTexture.Hide(0.5f);
		thrustTexture.Hide(0.5f);
		//start game!!!!!!!! YAAAAAAAAAAAAY
		//definitely pass this event up to the GAME MASTER ooOOoo (manager/controller w/e)
	}

	void HandleShowHide (object sender, EventArgs e)
	{
		if(hide)
		{
			startTexture.Hide(0.5f);
			hide = !hide;
		}
		else
		{
			startTexture.Show(0.5f);
			hide = !hide;
		}

	}
}

