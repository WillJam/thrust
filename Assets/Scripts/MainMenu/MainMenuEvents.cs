using System;


public class MainMenuEvents
{
	
	public event EventHandler<EventArgs> ControlSpacePressed;
	public void OnControlSpace()
	{
		var handler = ControlSpacePressed;
		if(handler != null) handler(this, EventArgs.Empty);
	}
	
	public event EventHandler<EventArgs> SpacePressed;
	public void OnSpacePressed()
	{
		var handler = SpacePressed;
		if(handler != null) handler(this, EventArgs.Empty);
	}
	
	public event EventHandler<EventArgs> ShowHideCycle;
	public void OnShowHideCycle()
	{
		var handler = ShowHideCycle;
		if(handler != null) handler(this,  EventArgs.Empty);
	}
}


