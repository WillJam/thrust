using System;
using UnityEngine;

public class MainMenuModel
{
	
	public Texture titleTexture = (Texture) Resources.Load ("Textures/title");
	public Vector3 titlePos = new Vector3(0.5f,0.6f,1);
	public Vector3 titleSize = new Vector3(0.7f,0.425f,1);
	
	public Texture pressStartTexture = (Texture) Resources.Load ("Textures/start");
	public Vector3 startPos = new Vector3(0.5f,0.35f,1);
	public Vector3 startSize = new Vector3(0.35f,0.09f,1);
}

 
