using System;
using UnityEngine;

public class MainMenuView :MonoBehaviour
{
	
	public MainMenuEvents Events = new MainMenuEvents();
	float cycle = 0.5f;
	public bool gameStart;
	public MainMenuView ()
	{
	}
	
	void Update()
	{
		if(Input.GetKeyDown(KeyCode.LeftControl))
		{
			Debug.Log ("YEAH");
			Events.OnControlSpace();
			cycle = 0.5f;
		} 
		else if(Input.GetKeyDown("space"))
		{
			Events.OnSpacePressed();
			cycle = 0.5f;
		}
		
		
		if(cycle > 0)
		{
			cycle -= Time.deltaTime;
		}
		else 
		{
			if(!gameStart)
			{
				Events.OnShowHideCycle();
			}
			else 
			{
				gameObject.SetActive(false);
			}
			cycle = 0.5f;
		}
	}
}


